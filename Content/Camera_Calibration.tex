%!TEX root = ../report.tex
\subsection[Camera Calibration]{Camera Calibration $\ast$} \label{ch::camera_calibration}
As described in \cref{ch:camera}, the Waveshare IMX219-170 camera was chosen for this project.
The first step is to describe the geometric calibration of the camera.
Subsequently, further adjustment of the camera parameters are mentioned.


To enable distance estimation for lanes and traffic signs, the camera needs to be calibrated.
Camera calibration includes the determination of all parameters for the mapping between the image and world coordinate system \cite{suse_bildverarbeitung_2014}.

However, this mapping can be divided into two steps:
\begin{enumerate}
    \item Projection from the image to the camera coordinate system.
    \item Projection from the camera to the world coordinate system.
\end{enumerate}
These two steps are explained using the pinhole camera model. Afterwards, the presented camera is calibrated and the problem of distortion caused by non-ideal lenses is solved. Finally, the achieved results are shown.

In the following, coordinates in the image coordinate system are called \textit{image coordinates}, while these in the world coordinate system are named \textit{object coordinates}.

\subsubsection{Pinhole Camera Model} \label{ch::pin_hole_model}
The pinhole camera model, shown in \cref{fig:pinhole_model}, is a widely used model to explain projection of cameras. 
It is based on the assumption that one object point is projected by exactly one ray -- passing through a very small aperture -- to the image plane \cite{suse_bildverarbeitung_2014}.
\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{./Pictures/pinhole_camera_model.png}
    \caption[Pinhole camera model]{Schematic representation of the pinhole camera model: The point P in the camera coordinate system (described by axes $X_C$, $Y_C$ and $Z_C$) is projected onto the point $(u, v)$ in the image coordinate system (described by axes $x$ and $y$) \cite{opencv_pinhole_model}.}
    \label{fig:pinhole_model}
\end{figure}

To simplify the explanation, the lens is assumed to be very thin.
In that case, the following equation can be derived from \cref{fig:pinhole_model} with the image point $\boldsymbol{\mathrm{P^C}}=(u, v)$ and the object point $\boldsymbol{\mathrm{P^W}}=(X_W, Y_W, Z_W)$, where subscript $H$ denotes representation in homogenous coordinates:

\begin{equation}
    \begin{gathered}
        s\cdot\boldsymbol{\mathrm{P_H^I}} = \boldsymbol{\mathrm{A}}\cdot[\boldsymbol{\mathrm{R}}|\boldsymbol{\mathrm{t}}]\cdot\boldsymbol{\mathrm{P_H^W}} \\
        s\cdot\boldsymbol{\mathrm{P_H^I}} = \boldsymbol{\mathrm{A}}\cdot\boldsymbol{\mathrm{E}}\cdot\boldsymbol{\mathrm{P_H^W}} \\
        s\cdot
        \begin{bmatrix}
            u \\
            v \\
            1 \\
            \end{bmatrix}
            =
            \begin{bmatrix}
                f_x     & 0     & c_x \\
                0       & f_y   & c_y \\
                0       & 0     & 1 
            \end{bmatrix}\cdot
            \begin{bmatrix}
                r_{11}  & r_{12}    & r_{13}    & t_{1} \\
                r_{21}  & r_{22}    & r_{23}    & t_{2} \\
                r_{31}  & r_{32}    & r_{33}    & t_{3}
            \end{bmatrix}\cdot
            \begin{bmatrix}
                X_W \\
                Y_W \\
                Z_W \\
                1
            \end{bmatrix}
        \label{eq:pinhole_equation}
    \end{gathered}
\end{equation}

The matrix $\boldsymbol{\mathrm{A}}$ denotes here the camera matrix, also called intrinsic matrix, and contains the focus point $(c_x, c_y)$ and the focal lengths in x- and y-direction $f_x$ and $f_y$.
Next to this, the extrinsic matrix $[\boldsymbol{\mathrm{R}}|\boldsymbol{\mathrm{t}}]$, also called $\boldsymbol{\mathrm{E}}$, consists of the rotation matrix $\boldsymbol{\mathrm{R}}$ and the translation vector $\boldsymbol{\mathrm{t}}$.
It projects a point from the camera coordinate system to the world coordinate system, which is fixed in relation to the camera.
Last, the scalar $s$ is a scaling variable which must be set if the image is scaled.
There are 11 independent parameters in total, five intrinsic and six extrinsic parameters. 
Therefore, one needs at least 6 images to calculate these parameters, because two equations can be derived from a single image.
However, the more images are provided, the better the calibration result will be \cite{suse_bildverarbeitung_2014}.

Due to the constant focal length \cite{imx219}, the estimated parameters need to be calculated only once and can be reused afterwards \cite{opencv_pinhole_model}. 

\subsubsection{Performing the Calibration} \label{ch::practical_calibration}
The presented camera was calibrated using a $10 \times 8$ checkerboard pattern commonly utilized for camera calibration.
First of all, pictures of this pattern were taken from several views, varying both the location and the orientation. In total 35 images were acquired and used for calibration.

After that, the OpenCV function \texttt{findChessboardCorners} \cite{opencv_pinhole_model} was used to identify salient points.
In addition, the \texttt{cornerSubPix} function of OpenCV was applied to refine the coordinates.
Subsequently, the positions of the detected points were checked, as illustrated in \cref{fig:chessboard_corners}.
Some of them were not recognized correctly. In consequence, the corresponding images, e.g. \cref{fig:chessboard_corners2}, were labeled manually.

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=0.98\linewidth]{./Pictures/corners_20.png}
        \caption{} 
        \label{fig:chessboard_corners1} 
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=0.98\linewidth]{./Pictures/corners_0002.png}  
        \caption{}
        \label{fig:chessboard_corners2} 
    \end{subfigure}
    \caption[Detected points for variable pattern orientations]{Detected points for variable pattern orientations, generated with OpenCV's \texttt{drawChessboardCorners}. Corners in (a) are automatically detected by the function \texttt{findChessboardCorners}, corners in (b) are set manually.}
    \label{fig:chessboard_corners}
\end{figure}

Next, the actual calibration function of OpenCV, namely \texttt{calibrateCamera}, was to be applied. In order to do so, corresponding object points had to be defined as well. Here, x- and y-coordinates were calculated by the known size of the squares and the row and column counts, respectively. However, the z-value had to be the same for all points due to the function's limitation to only compute camera parameters for planar calibration rigs. 

The function \texttt{calibrateCamera} at first determines the initial intrinsic parameters, afterwards estimates the pose and finally runs the Levenberg-Marquadt optimization algorithm to minimize the reprojection error.
The reprojection error is computed as the distance between the actual coordinates of image points and the projected image points. The latter can be computed by applying \cref{eq:pinhole_equation} -- with the determined parameters inserted -- to the pre-defined object points.
The optimization also includes the determination of distortion coefficients, which will be introduced in the next section.

\subsubsection{Undistortion of Images} \label{ch::undistortion}
Due to non-ideal lenses and wide-angle objectives in cameras, different areas in images are distorted differently.
To remove these imperfections from the final image, which e.g. is used for distance estimation, the image needs to be undistorted. 

First, one must distinguish between radial and tangential distortion \cite{opencv_camera_calib}. 
On the one hand, radial distortion is especially a problem with fisheye / wide-angle cameras.
It causes actually straight lines to appear in images as curves. 
The further away a point is from the centre of the image, the stronger the distortion is \cite{suse_bildverarbeitung_2014}. 
\Cref{fig:chessboard_corners2} shows, that the chosen camera, which has a 170$^\circ$ field of view, acquires images with noticeable distortion. 

On the other hand, there is tangential distortion, which is caused by non-parallelity of image plane and the lens. 
Despite the fact that tangential distortion's influence is less, it should also be taken into account.

OpenCV's function \texttt{calibrateCamera} computes parameters for both, in detail 3 parameters describing radial distortion and 2 defining tangential distortion \cite{opencv_camera_calib}. Furthermore, the OpenCV function \texttt{getOptimalNewCameraMatrix} computes the optimal intrinsic matrix based on a free scaling parameter \cite{opencv_pinhole_model}. 
Additionally, it returns the new rectangular \ac{roi}, which will be explained later on.

Finally, this newly calculated matrix as well as the distortion coefficients can be used to create an undistorted image, accomplishing the mapping from the initial pixel coordinates to the final pixel coordinates in the undistorted image using the function \texttt{initUndistortRectifyMap} of OpenCV. 
This projection is a forward projection and projects the regular pixel grid from the initial image onto a possibly non-regular grid in the target image.
As a consequence, the actual pixel values in the target image need to be interpolated \cite{suse_bildverarbeitung_2014}.

\subsubsection{Interpolation and Extraction of ROI} \label{ch::interpolation_roi}
The actual remapping from the non-regular grid in the target image to the regular grid can be performed by running OpenCV's \texttt{remap} function.
It allows usage of various interpolation algorithms, e.g. nearest-neighbor, bilinear and bicubic interpolation, which are now to be compared in terms of quality and performance.

\begin{figure*}
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Pictures/interpolation/lanes_distorted.png}
        \caption{}   
        \label{fig:lanes_distorted}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{Pictures/interpolation/lanes_interp_nearest.png}
        \caption{}     
        \label{fig:lanes_nearest}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}   
        \centering 
        \includegraphics[width=\textwidth]{Pictures/interpolation/lanes_interp_cubic.png}
        \caption{}      
        \label{fig:lanes_cubic}
    \end{subfigure}
    \begin{subfigure}[b]{0.475\textwidth}   
        \centering 
        \includegraphics[width=\textwidth]{Pictures/interpolation/lanes_interp_linear_marked.png}
        \caption{}     
        \label{fig:lanes_linear}
    \end{subfigure}
    \hfill
    \caption[Distorted image and undistorted images for various interpolation algorithms]{Distorted image and undistorted images of lanes for various interpolation algorithms. (a) distorted image to interpolate, (b) undistorted image with nearest-neighbor interpolation, (c) undistorted image with bicubic interpolation, (d) undistorted, cropped image with bilinear interpolation (red rectangle marks area used in figure~\ref{fig:interpolation_difference}).} 
    \label{fig:interpolation_results}
\end{figure*}

In order to carry out a performance test that is as close to the actual use case as possible, an image which contains lanes to be detected is used.
Thus, the before acquired projection matrices are applied on a distorted input image of size $640 \times 360$ pixels, which is presented in \cref{fig:lanes_distorted}.
The average time and standard deviation for $1000$ executions of each interpolation is shown in \cref{tab::interpolation_benchmark}.

\begin{table}[ht]
    \begin{tabularx}{\textwidth}{|X||X|X|}
        \hline
        \textbf{Algorithm}      & \textbf{Average time [ms]}   & \textbf{Standard deviation [ms]} \\\hline
        Nearest-neighbor    & 1.493459  & 0.605107      \\\hline
        Bilinear            & 3.292379  & 0.947716      \\\hline
        Bicubic             & 6.452399  & 1.297158      \\\hline
    \end{tabularx}
    \caption[Benchmark for selected interpolation algorithms]{Benchmark on the NVIDIA Jetson Nano for OpenCV's \texttt{remap} function with nearest-neighbor, bilinear and bicubic interpolation. Performance tests consist of 1000 executions per interpolation algorithm on a $640 \times 360$ px distorted image.}
    \label{tab::interpolation_benchmark}
\end{table}

First, nearest-neighbor interpolation uses the value of the nearest neighbor pixel.
Therefore, it is the fastest of the considered interpolation algorithms.

Second, the bilinear interpolation algorithm computes the value of the pixel in the target image by constructing two straight lines, one in column and one in row direction. 
Thereby, the values of the immediate neighbors in a $2 \times 2$ area of the initial image are used to determine the value of the desired position in the target image.
Both the value derived in x- and the one derived in y-direction are combined to the final value.
Thus, this interpolation method is slower than the nearest-neighbor algorithm.

Last, the bicubic interpolation operates in contrast to the bilinear method on the $4 \times 4$ instead of the $2 \times 2$ neighborhood.
Additionally, it uses a third-order interpolation to determine the final value \cite{matlab_image}.
The increasing complexity across the above algorithms is reflected by the longer runtimes in \cref{tab::interpolation_benchmark}.

\begin{figure*}
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Pictures/interpolation/lanes_dif_linear_nearest.png}
        \caption{}   
        \label{fig:diff_linear_nearest}
    \end{subfigure}
    \hspace{0.2\textwidth}
    \begin{subfigure}[b]{0.25\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{Pictures/interpolation/lanes_dif_linear_cubic.png}
        \caption{}     
        \label{fig:dif_linear_cubic}
    \end{subfigure}
    \caption[Difference images of nearest-neighbor and bicubic interpolation to linear interpolation]{Absolute difference images for the area marked by the red rectangle in figure~\ref{fig:lanes_linear} between the bilinear interpolated image and (a) the nearest-neighbor interpolated image; (b) the bicubic interpolated image. To increase the contrast, both images were multiplied by the same factor.} 
    \label{fig:interpolation_difference}
\end{figure*}

As one can recognize by the dark stripe in \cref{fig:diff_linear_nearest}, nearest-neighbor interpolation causes stairs-like effects.
Since straight lines are essential for later processing in relation to lane detection, this type of interpolation does not satisfy the requirements, despite showing the highest performance.

Bilinear and bicubic interpolation methods lead to very similar results, illustrated by the absolute difference image in \cref{fig:dif_linear_cubic}.
Due to the lower calculation time bilinear interpolation is eventually chosen as interpolation algorithm for undistortion.

For the black pixels in \cref{fig:lanes_linear}, no values could be determined, as they are projected onto a position out of the distorted image.
To avoid image corruption in further processing steps, these areas must be cut off. Thus, a \ac{roi} is to be determined. As mentioned in the last section, OpenCV's function \texttt{getOptimalNewCameraMatrix} returns position and size of this \ac{roi}. Hence, any distorted image of size $640 \times 360$, e.g. the one shown in \cref{fig:lanes_distorted}, is undistorted and finally cropped to a size of $607 \times 313$ pixels.
The image can now be used to determine the object coordinates from the pixel coordinates.

\subsubsection{Mapping from Image Coordinates to World Coordinates}
\Cref{eq:pinhole_equation} shows the determination of image coordinates based on the known world coordinates of an object.
For the following applications, however, it is necessary to determine the world coordinates on the basis of the calculated image coordinates. 

The conversion of this equation according to the world coordinates $\boldsymbol{\mathrm{P_H^W}} = (X_W, Y_W, Z_W)$ leads to \crefrange{eq:img_to_cam_coordinates}{eq:cam_to_world_coordinates}, where $\boldsymbol{\mathrm{P^C}}$ denotes the coordinates in the intermediate used camera coordinate system:

\begin{equation}
    \boldsymbol{\mathrm{P^C}} = s\cdot\boldsymbol{\mathrm{P_H^I}}\cdot\boldsymbol{\mathrm{A}}^{-1}
        \label{eq:img_to_cam_coordinates}
\end{equation}
\begin{equation}
    \boldsymbol{\mathrm{P_H^W}} = \boldsymbol{\mathrm{P_H^C}}\cdot\boldsymbol{\mathrm{E_H}}^{-1}
    \label{eq:cam_to_world_coordinates}
\end{equation}

Since the calibration pattern in \cref{fig:chessboard_corners2} was placed almost directly on the ground, the transformation matrices determined for it can be used to transfer image coordinates to world coordinates of points on the ground.
Due to the imperfect camera calibration, points that are actually in the plane of the calibration pattern, i.e. the x-y plane, are not projected exactly into it. Therefore, it is necessary to determine the intersection of the straight line from the camera to the projected world point and the x-y plane.

The coordinate system in which this point is located must finally be translated and rotated in such a way that the origin of the final coordinate system is below the camera on the ground, since this point has been set as the reference point for the vehicle.

\subsubsection{Calibration Results} \label{ch::calib_results}
A total of 35 images were used, some of which had to be labeled manually.
Despite decreased precision due to manual labeling, the final average reprojection error is 0.033 px.
Considering all images, the calibration yields a maximal reprojection error of 0.080 px.

In consequence, for the leftmost point in the green row of the manually labeled image in \cref{fig:chessboard_corners1}, the deviation between the actual object coordinate and the computed one is only 2.5mm, while it is just under 3mm for the rightmost corner in this row.
These values where calculated by applying the above equation to the previously determined image coordinates.
However, significantly larger deviations were found for points outside the calibration pattern in \cref{fig:chessboard_corners2}, which may be caused by the not fixed camera position on the car and less images with the same setting as in \cref{fig:chessboard_corners2}, but varying distances.

\subsubsection{Additional Camera Parameters} \label{ch::camera_parameters}
The capturing of images with the aforementioned camera is set up using a GStreamer pipeline.
\textit{GStreamer} is a framework for creating multimedia streams \cite{gstreamer} that is embedded into the existing software on the NVIDIA Jetson Nano by the \textit{libargus} library \cite{jetson_libargus}.
The latter allows reading and setting various properties of the connected cameras.

\begin{figure*}
    \centering
    \begin{subfigure}[b]{0.475\textwidth}
        \centering
        \includegraphics[width=\textwidth]{Pictures/white_balancing/0_linear.png}
        \caption{}   
        \label{fig:wb_off}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}  
        \centering 
        \includegraphics[width=\textwidth]{Pictures/white_balancing/1_linear.png}
        \caption{}     
        \label{fig:wb_auto}
    \end{subfigure}
    \vskip\baselineskip
    \begin{subfigure}[b]{0.475\textwidth}   
        \centering 
        \includegraphics[width=\textwidth]{Pictures/white_balancing/2_linear.png}
        \caption{}     
        \label{fig:wb_incandescent}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.475\textwidth}   
        \centering 
        \includegraphics[width=\textwidth]{Pictures/white_balancing/3_linear.png}
        \caption{}      
        \label{fig:wb_fluorescent}
    \end{subfigure}
    \caption[Comparison of white balancing modes provided by the camera]{Comparison of different white balancing modes provided by the IMX219-170 camera. (a) white balancing turned off, (b) white balancing auto mode, (c) incandescent white balancing mode, (d) fluorescent white balancing mode.} 
    \label{fig:wb_comparison}
\end{figure*}

Since the vehicle is adapted to indoor environments, as described in the introduction, the camera parameters can be adapted to these environmental conditions too.
For this purpose, the \textit{libargus} library enables the user to select a white balancing mode from the set provided by the IMX219-170 camera.
\Cref{fig:wb_comparison} compares images acquired with different white balancing modes respectively white balancing turned off.
Images acquired with fluorescent white balancing mode, auto white balancing or deactivated white balancing mode show to have a tint reaching from yellow to orange.

To avoid yellowish images, the white balance mode parameter is set to \textit{incandescent}, which is shown in \cref{fig:wb_incandescent}.
Additionally, exposure time, gain and adjustment of digital gain are fixed to specific values to accomplish stable lighting conditions \cite{accelerated_gstreamer_doc}.
Finally, an adjustment of the saturation proves to be necessary due to the image processing algorithms presented in the later sections.
