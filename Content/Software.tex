%!TEX root = ../report.tex
\section{Software Frame} \label{ch::software}

In this chapter, a software frame is developed to facilitate the implementation of \ac{ai}-based vehicle control. First, a distributed system architecture is established in \cref{ch::client_server_arch}, enabling remote control as well as observation of the vehicle. \Cref{ch::implementation_client} subsequently deals with the implementation of the software frame on the NVIDIA Jetson Nano, particularly the efficient parallelization of tasks. In \cref{ch::transmission_protocol}, a transmission protocol for client-server-communication is developed. Eventually, \cref{ch::implementation_server} covers the remote-side implementation of the software frame, including a \ac{gui}.

\subsection[Distributed System Architecture]{Distributed System Architecture $\ddagger$} \label{ch::client_server_arch}

To understand the driving behavior and detect potential malfunction of the autonomous vehicle, one needs a system that both shows sensor inputs and explains subsequent actions. Moreover, the vehicle should stop very quickly in case of e.g. heading for an unrecognized collision, which is probably difficult to be done by pushing a physical button on the vehicle, considering shortness of time and spatial distance from hardware.

One approach to achieve the goal of having more control is to extend the console output of the software running on the NVIDIA Jetson Nano. This is, however, a two-edged sword, as increasing the amount of output information goes along with reduced clarity. In addition, console output is known to be quite inflexible, e.g. images cannot be depicted and switching between different levels of verbosity by pressing hot keys often turns out to be inconvenient as well.

Another and more preferable approach is to set up a distributed system architecture in the sense of a client/server model. Here, the autonomous vehicle sends its sensor data as well as the actions resulting therefrom to a remote computer, which presents information in a clear and flexible \ac{gui}. On the other hand, the remote application provides graphical control elements to perform several actions. Specifically, it is intended to provide start/stop commands as well as an interface to control the vehicle manually, by use of a game controller.

The terminology of “server” and “client” can be ambiguous in the given use case. \citeauthor{schillVerteilteSystemeGrundlagen2012} define a server as the provider and a client as the user of a specific service \autocite{schillVerteilteSystemeGrundlagen2012}. Here, it depends on which functionality is considered the service: On the one hand, the autonomous vehicle delivers information about its sensor data and the actions, and on the other hand, the remote software provides some control facilities. As a result, there is a juxtaposition of data server and control server. But since the latter is eventually able to influence the former and hence is to be seen superior, the remote application is considered as the actual server and the autonomous vehicle represents the client in this distributed system architecture.


\subsection[Client-side Implementation]{Client-side Implementation $\ddagger$} \label{ch::implementation_client}

\subsubsection{Task Identification} \label{ch::task_identification}

Since autonomous driving encompasses a wide range of possible requirements and features, the first step is to define the scope of action in which the developed vehicle is to operate. First of all, it should be able to move between lanes without crossing them. Besides that, the vehicle is intended to recognize traffic lights as well as a limited set of traffic signs and behave accordingly. Last, it should be able to detect objects directly in front of it and prevent a collision by stopping in time.

To realize these features, a camera and three ultrasonic sensors can be used as sensoric inputs (see \cref{ch:sensor_selection,ch:setup_ultrasonic_sensor,ch:camera}). Moreover, an \ac{esc} and a servo motor can be addressed as actors to control speed respectively steering of the vehicle (see \cref{ch:hardware_assembly}).

So the first task of the software on the NVIDIA Jetson Nano is to capture images with the camera and make them available to the other modules. Covering the second kind of sensoric input, another task is to measure distances to potential objects in front of the vehicle. In addition, with access to the acquired images, lane detection and traffic sign/lights recognition can be considered as further tasks. To react in a coordinated and timely manner, the latter also includes distance estimation of the recognized traffic signs and lights. Trajectory planning, based on the collected data and subsequent vehicle controlling, is another task, being essential to act at all. Last, data has to be exchanged between the server and the client, implying tasks for data receiving and sending. This one is actually special, compared with the former ones, as it is not mandatory to achieve described autonomous driving capabilities, but enhances the comprehensibility of the vehicle's actions and provides the user more control (see \cref{ch::client_server_arch}).


\subsubsection{Task Dependence Analysis} \label{ch::task_dependency_analysis}

All those tasks mentioned in \cref{ch::task_identification} could be performed one after each other per working cycle and the total system would work in principle. However, \grqq working in principle\grqq{} is a weak statement and does not consider all dimensions of the problem. It's self-evident that strictly sequential execution leaves resources unused, hence does not enable the highest-possible performance. But, foremost and particularly in view of the limited computational resources on the NVIDIA Jetson Nano and the variety of tasks, it does not guarantee sufficiently high performance to meet the requirements of the given use case for cycle frequency: If the system would be only able to scan its environment e.g. once per second, its locomotion speed might be too high in order to react to traffic signs, hold lanes and avoid collisions. Of course, this could be counterbalanced by reducing the locomotion speed adequately, but at some point, this would reduce flexibility too much, allowing only very few distinct speeds. Besides, even the lowest-possible locomotion speed could still be too high in extreme cases.

It's more efficient and performant to parallelize tasks as much as possible. To do so, the dependencies of tasks must be determined first, as tasks can only be parallelized, if they do not depend on each other. A \ac{dag} is often used to model such dependencies \autocite{terrellConcurrencyNETModern2018}.

\begin{figure}
    \subcaptionbox{\acl{dag}: Task dependencies per working cycle\label{fig::task_dependency_dag}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/dag.gv.pdf}}
    \hspace{\fill}
    %
    \subcaptionbox{Directed Cyclic Graph: Task dependencies across working cycles\label{fig::task_dependency_dg}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/dg.gv.pdf}}

    \subcaptionbox{Directed Cyclic Graph: Task dependencies across working cycles (extended)\label{fig::task_dependency_dg_en}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/dg_en.gv.pdf}}
    %
    \caption[Visualization of task dependencies for autonomous driving as directed graphs]{Visualization of task dependencies for autonomous driving as directed graphs. \Cref{fig::task_dependency_dag} shows the intra-cycle task dependencies as a \acl{dag}, \cref{fig::task_dependency_dg} shows them from inter-cycle perspective, resulting in a \acl{dag}. Finally, \cref{fig::task_dependency_dg_en} shows inter-cycle dependencies as well, including the auxiliary task \grqq Copy Data\grqq. In all cases, nodes represent tasks and edges represent dependencies. Moreover, starting and ending nodes are depicted by white respectively black circles.}
    \label{fig::task_dependency_graphs}
\end{figure}

In \cref{fig::task_dependency_dag}, task dependencies for the given problem of autonomous driving\footnote{For the sake of clarity, the aforementioned manual control functionality is not represented here.} are visualized by a \ac{dag}. It's important to mention that this model is constructed from intra-cycle perspective, i.e. taking solely dependencies within a single working cycle into account. One can see that both Traffic Sign Recognition and Lane Detection are dependent on the Image Acquisition task, but are themselves independent of each other and consequently can run concurrently. Furthermore, Collision Detection and Data Receiving tasks do not have to run in strict sequential order. While the former precedes the Vehicle Controlling task and therefore has to be involved in task synchronization, the latter only determines the vehicle stop and subsequent program termination here.

Although intra-cycle dependence analysis using a \ac{dag} already provides some parallelization approaches, its scope is limited and does not allow to recognize the whole complexity. Hence, the graph is extended by inter-cycle dependencies, which results in a Directed Cyclic Graph, as shown in \cref{fig::task_dependency_dg}. Specifically, the only differences from \cref{fig::task_dependency_dag} are the removal of the transition from the Data Sending task to the end node and the addition of the transition from the Data Sending task to the Image Acquisition task, inducing a new working cycle.

However, starting a new working cycle only after data transfer is complete has potential for optimization, as network transfer speeds can limit cycle frequency, making data transfer a significant bottleneck for computing resources. Actually, the Data Sending task can be encapsulated from the rest of the cycle elements by starting a new cycle immediately after the Vehicle Controlling task. This brings up another problem, though: Data to be transmitted may be overwritten by the activities of the subsequent cycle. This is solved by adding an auxiliary task named \grqq Copying Data\grqq, creating copies of the relevant data before the next cycle is started. The Data Sending task is then to send the copies, meanwhile the subsequent cycle can overwrite original data. The resulting dependency graph is shown in \cref{fig::task_dependency_dg_en}.


\subsubsection{Task Sequence Plan} \label{ch::task_sequence_plan}

Based on the determined dependency graph, a task sequence plan, i.e. a schema that describes the way tasks and task-inherent actions follow each other, can be created. Here, the manual driving mode, which has been omitted so far, is also of interest. To model this sequence plan, a notation similar to \ac{bpmn} \autocite{InformationTechnologyObject2013} is chosen. It consists of horizontal swim lanes representing tasks and activities determining task-inherent actions. Moreover, transitions between tasks are denoted by events being activated for the started task.

Starting with the Image Acquisition task as a working cycle's first task, a new image is captured, independent of the control mode. Then, both the Lane Detection and the Traffic Sign Detection tasks can be entered, operating on the previously acquired image. The latter includes some distance estimation of the detected traffic signs as well. After that, the path forks for the first time, as autonomous driving and manual driving require different subsequent tasks. For autonomous driving, the Vehicle Controlling task and the trajectory planing are scheduled based on results of the two preceding tasks. For manual driving on the other hand, vehicle controlling is not determined by image processing, so the corresponding task is skipped. In both cases, however, both the Lane Detection task and the Traffic Sign Detection task must be completed in order to proceed.

At this point, one might ask why the Collision Detection task does not have to be performed concurrently to the image processing tasks, as the dependence analysis in \cref{ch::task_dependency_analysis} suggests. Actually, in an ideal world, Collision Detection would be incorporated that way. But the used ultrasonic sensors turned out to be not as accurate and in particular stable as intended (see \cref{ch:ultrasonic_accuracy}), consequently the task needs to be performed perpetually and a small number of measurements must be averaged.

Since only start respectively stop and mode changing commands are received in autonomous driving mode, the Data Receiving task is performed perpetually as well. In manual driving mode on the contrary, the vehicle also receives control parameters. Hence, the Data Receiving task and the Vehicle Controlling task form a separate cycle in the task sequence plan.

Independent of the driving mode, the collected and computed data needs to be transmitted to the server instance. However, the concrete moment depends on it. In the case of autonomous driving, this occurs after completion of the Vehicle Controlling task, and in the case of manual driving, after completion of the Lane Detection and Traffic Sign Recognition tasks. As already mentioned, the data transmission can become a bottleneck when the network speed for data transmission at working cycle frequency is too low. Therefore, a check whether last cycle's data is still being sent is introduced. If it is, both data copying and data transmission for this cycle can be skipped and the next one starts immediately. If it is not, the latest data is copied and the next cycle as well as the Data Sending task start in parallel.

\begin{figure}
    \centering
    \includegraphics[width=0.95\textwidth, keepaspectratio]{Pictures/task_sequence_plan.pdf}
    \caption[Visualization of the task sequence plan]{Visualization of the task sequence plan. The used notation is similar to \ac{bpmn} \autocite{InformationTechnologyObject2013}: Horizontal swim lanes represent tasks and activities represent task-inherent actions. Moreover, transitions between tasks are denoted by events being activated for the started task. Red arrows denote action flow for autonomous driving mode, while blue arrows do so for manual driving mode. Eventually, green arrows indicate action flow shared by both modes.}
    \label{fig::task_sequence_plan}
\end{figure}

The final task sequence plan is shown in \cref{fig::task_sequence_plan}.


\subsubsection{Parallelization Approaches} \label{ch::parallelization_approaches}

There are different approaches to parallelize tasks in practice. The most simple one might be using several threads, each of which performs a single task. However, as Python is used as a programming language, one has to take account of its peculiarities. In case of Python, the so-called \ac{gil} has to be considered, which is a mechanism on interpreter level that ensures thread-safe code execution. What sounds great at first, suggesting thread-safe resource access, actually turns out to be a problem: Thread-safety is achieved by limiting the number of concurrently running threads to one. Thus, code \grqq parallelized\grqq{} by threads is actually not parallelized at all, but solely avoids blocking of the whole program if a single thread blocks \autocite{gorelickHighPerformancePython2020}.

Using Python, the only method to circumvent the \ac{gil} and execute code concurrently is to start several processes. At this point, the next problems arises: Besides having more overhead and being less lightweight than threads, different processes have different address spaces, necessitating \ac{ipc} techniques to share data. Furthermore, one needs to ensure synchronized access to shared resources, otherwise deadlocks may occur \autocite{gorelickHighPerformancePython2020}.

To create and maintain several processes, Python provides the module \texttt{multiprocessing} \autocite{MultiprocessingProcessbasedParallelism}, containing the class \texttt{multiprocessing.Process}. On a UNIX based operating system like NVIDIA Jetson Nano's Ubuntu, a new Python process is created as a fork of the creating process. There are two main classes of \ac{ipc} techniques to share data, namely being \textit{message based} and \textit{shared memory based} \autocite{wolfLinuxUnixProgrammierungUmfassendeHandbuch2016}.

First, message based methods use e.g. access-safe \ac{fifo} queue implementations, where one process puts data into a pipe and another one receives it. In order to be sent via pipe, data has to be serialized, which is done by the module \texttt{pickle} \autocite{PicklePythonObject} in Python. Usually, the message based approach is preferable, as it is easier to use and less prone to produce race conditions. To perform massage based \ac{ipc}, Python provides for example the class \texttt{multiprocessing.Queue} \autocite{MultiprocessingProcessbasedParallelism}.

Second, with the shared memory based method, memory being allocated by some process can be made known to another process, adding its address to the address space of the latter. Shared memory based \ac{ipc} is tendentially faster than message based one, as data does not have to be copied between processes. On the contrary, access logic may become cumbersome and synchronization generally has to be performed manually to avoid deadlocks and race conditions. The Python module \texttt{multiprocessing}, however, contains both types that are already access-safe by implicit registration of locks and ones that enforce unsafe access of data. Depending on the plurality of data, the types \texttt{multiprocessing.Value} and \texttt{multiprocessing.Array} respectively \texttt{multiprocessing.sharedctypes.RawValue} and \texttt{multiprocessing.sharedctypes.RawArray} can be used \autocite{MultiprocessingProcessbasedParallelism}.

To determine the actual performance difference of both parallelization approaches in light of the autonomous driving use case, a benchmark was performed. Below, some issues are stated that substantially influenced the design as well as the execution of the benchmark.

First, as the results of the benchmark are to influence the way process synchronization is ultimately implemented on the NVIDIA Jetson Nano, the benchmark should also run on the aforementioned system to rule out potential system or environment related influences. Used software packages are therefore equivalent to those being used in practice later on and are stated in \ref{ch::add_used_software}.

Second, analysis of data to be shared by different processes leads to the hypothesis that image data may have a greater impact on performance compared to the rest of the data due to the difference in size: An image captured with the resolution $640\times360$ px, RGB channels and 8 bit depth takes about 675 KBytes in memory, while e.g. the vehicle controlling parameters are only two 8 bit variables. Hence, evaluating performance particularly in the scope of such data sizes is of great interest. Nevertheless, in order to evaluate scalability as well, performance is to be compared on a more general basis, considering several orders of magnitude in terms of data size. In detail, array sizes from 10 to 1000000 are tested, with the range being scaled logarithmically and a single array element being a 32-bit integer.

Third, a metric is needed to measure performance. Since the actual processes for autonomous driving are to run cyclically later on, it makes sense to run processes cyclically in the benchmark as well and to use the cycle frequency as a performance metric. Doing so, the number of processes is another variable on which the performance may depend and that is to be varied in the benchmark. As the NVIDIA Jetson Nano has only 4 \ac{cpu} cores and can run the same number of processes concurrently, 4 processes might be enough. However, according to the task sequence plan, 6 different processes are to deal with the image data, so the benchmark should test performance for both 4 and 6 processes as well as for 2 and 8 processes, with the latter giving an outlook on scalability.

Fourth, to determine the speed differences of data transmission between various processes as accurate as possible, other process internal actions should be reduced to a minimum. Therefore, only a single value is incremented per process iteration, which is necessary to keep track of the number of completed cycle passes.

Last, a single benchmark configuration, i.e. a specific assignment of variable parameters has to run sufficiently long to exclude the influence of random disturbance variables. 60 seconds is considered as an adequate duration, taking into account that frequency is measured in $s^{-1}$. In order to reduce especially the influence of larger-sized disturbance variables, every benchmark configuration is also to be run five times and only the average cycle frequency is taken as actual result. Furthermore, the standard deviation of the five runs is to be evaluated.

To sum it up, the benchmark compares the performance of message based \ac{ipc} against the shared memory based approach. It is implemented in Python, executed on the NVIDIA Jetson Nano and takes use of the module \texttt{multiprocessing}, in particular the classes\\\texttt{multiprocessing.Process} and \texttt{multiprocessing.Queue} as well as the type \\\texttt{multiprocessing.sharedctypes.RawArray}. A single benchmark configuration is defined by the used parallelization approach, the array size (varying in range $\{10, 100, ..., 1000000\}$ at constant element size of 32 bit) and the process count (varying in range $\{2, 4, 6, 8\}$). Every benchmark configuration is run for 60 seconds at 5 repetitions. Finally, the cycle frequency is used to measure the performance.

\begin{figure}
    \subcaptionbox{2 processes\label{fig::benchmark_p2}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/benchmark_p2.pdf}}
    \hspace{\fill}
    %
    \subcaptionbox{4 processes\label{fig::benchmark_p4}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/benchmark_p4.pdf}}

    \subcaptionbox{6 processes\label{fig::benchmark_p6}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/benchmark_p6.pdf}}
    \hspace{\fill}
    %
    \subcaptionbox{8 processes\label{fig::benchmark_p8}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/benchmark_p8.pdf}}
    \caption[Visualization of benchmark results with respect to parallelization approaches]{Visualization of benchmark results w.r.t. parallelization approaches. The blue graph shows performance of message based \ac{ipc} based on queues, the red one does for the shared memory approach. On the x-axis, the size of the communicated array is plotted. On the y-axis, the achieved cycle frequency is plotted in Hz. Various plots show performance for different process counts. Error bars indicate the standard deviation per measurement.}
    \label{fig::benchmark}
\end{figure}

\Cref{fig::benchmark} shows the benchmark results as graphical plots. First and foremost, evaluation of the results reveals that shared memory based \ac{ipc} outclasses the message based approach in every tested scenario. Moreover, the cycle frequency remains constant for increasing array sizes here, while decreasing in case of message based communication. For increasing process counts, relative performance differences between the two approaches remain almost constant at first glance, but actually slightly decrease; the absolute values in cycle frequency, on the contrary, seem to be approximately inversely proportional to the process count.

Considering the most relevant process count scenario with 6 processes, the shared memory based \ac{ipc} is at least 3.45 times faster than the message based one (with array size 10). In the extreme scenario with array size $10^6$, the performance gain even increases to a factor of 193.01. As mentioned above, the captured images take about 675 KBytes in memory and are therefore to be sorted between array sizes of $10^5$ and $10^6$ \footnote{Remember that a single array element requires 4 Bytes.}. The observed performance difference is highly significant here and justifies choosing the shared memory approach for \ac{ipc} in practice, despite greater effort as well as attention being required during implementation.

Finally, some reasons for the given benchmark results are to be found, if possible. As for the performance consistency of the shared memory based approach across different array sizes, it is self-evident since no data needs to be copied and a constant number of bytes is written per cycle, regardless of the array size. Furthermore, the significant performance decrease between array sizes of $10^3$ and $10^4$ using queues might be caused by the buffer size limit of a pipe on a UNIX system, which is internally used for \ac{ipc} here: Maximum buffer size is 65536 Bytes \autocite{kerriskLinuxProgrammingInterface2010}, apparently array sizes of 40000 Bytes might already lead to temporary blockage and hence delays. Last, as the load per cycle is proportional to the number of processes and the load per process in a specific cycle is constant, the cycle frequency would optimally be exactly inversely proportional to the process count. However, there seems to be some overhead, either of computational or communicational nature, leading to an overproportional decrease of the cycle frequency. Interestingly, this overhead is of different influence for the shared memory and the message based \ac{ipc}, leading to the aforementioned slight relative performance decrease for increasing process counts. Comparing e.g. the average performance decreases across array sizes from 2 processes to 6 processes, shared memory based \ac{ipc} becomes 4.23 times slower, while the message based approach becomes only 3.44 times slower. Optimally the factor would be 3 for both. It is unclear why this is the case -- further investigation would be required to draw conclusions. But as not being in the scope of this work, sole evaluation of the results is principally sufficient to make a decision on the most suitable parallelization approach for implementation. For the given use case of 6 processes and large array sizes between $10^5$ and $10^6$, shared memory clearly turns out to be the approach of choice.

\subsection[Network Protocols]{Network Protocols $\ddagger$} \label{ch::transmission_protocol}

Building up a distributed system architecture requires definition of protocols used for network communication. First, one has to distinguish between transport protocols like the \ac{tcp} or \ac{udp}, which are part of the transport layer in the \ac{osi} reference model, and message protocols belonging to the application layer \autocite{luntovskyyModerneRechnernetzeProtokolle2020}. Regardless of the concretely used transport protocol, there is need for a message protocol known for both client and server. Actually, the data to be transmitted from the client to the server is different from that transmitted the other way around, as the client sends status information and the server sends control commands. Therefore, two different message protocols, one for each direction, are necessary.

Concerning the communication from client to server, the autonomous vehicle is to transmit large amounts of image data besides ordinary, singular integers/floating-points/strings. This fact makes high demands on the transport protocol at first instance, as e.g. \ac{tcp} ensures packets to be delivered correctly and in the right order, but only allows packet sizes of max. 64 KBytes, requiring manual data recomposition. \ac{udp} on the other hand, though being more lightweight, has the same size restrictions and neither provides error correction nor flow control \autocite{luntovskyyModerneRechnernetzeProtokolle2020}.

% TODO: Update protocol (e.g. multiple lanes, object certainty instead of distance)
\begin{listing}
    \begin{minted}[frame=lines, linenos=true, autogobble=true, breaklines, tabsize=2]{JSONC}
    {
        "TIME": Client-side timestamp,
        "ENGINE": Speed parameter (servo angle for engine),
        "STEER": Steering parameter (servo angle for steering),
        "US": Ultrasonic sensor data
        {
            "FRONT": Distance measured by the front-sided sensor,
            "RIGHT": Distance measured by the right-sided sensor,
            "REAR": Distance measured by the rear-sided sensor
        },
        "LANE_DETC": Lane detection data (per detected object)
        {
            "X1": X coordinate of the first point of the lane,
            "Y1": Y coordinate of the first point of the lane,
            "X2": X coordinate of the second point of the lane,
            "Y2": Y coordinate of the second point of the lane
        },
        "OBJ_DETC": Object detection data (per detected object)
        {
            "NAME": Name of the detected object,
            "CERT": Estimated accuracy,
            "BB_X0": X coordinate of bounding box's bottom-left point,
            "BB_Y0": Y coordinate of bounding box's bottom-left point,
            "BB_X1": X coordinate of bounding box's top-right point,
            "BB_Y1": Y coordinate of bounding box's top-right point
        }
    }
    \end{minted}
    \caption[Network protocol from client to server as JSON object]{Network protocol from client to server as \ac{json} object. Keys are colored green, being followed by some explanation.}
    \label{lst::json_to_server}
\end{listing}

\begin{listing}
    \begin{minted}[frame=lines, linenos=true, autogobble=true, breaklines, tabsize=2]{JSONC}
        {
            "TIME": Server-side timestamp,
            "MAN_CTRL": Manual control flag,
            "ENGINE": Steering parameter (servo angle for engine),
            "STEER": Steering parameter (servo angle for steering),
            "STOP": Stop flag
        }
    \end{minted}
    \caption[Network protocol from server to client as JSON object]{Network protocol from server to client as \ac{json} object. Keys are colored green, being followed by some explanation.}
    \label{lst::json_to_client}
\end{listing}

\Ac{zmq} is a message-based network library, whose transport protocol \ac{zmtp}, among other features, does not have such a rigid packet size restriction\footnote{The maximum frame size is $2^{63}-1\approx 8\cdot10^9$ GBytes \autocite{hintjensZeroMQMessageTransport}.} \autocite{hintjensZeroMQMessagingMany2013}. Another appreciable feature is the inherent support of different messaging patterns, such as REQ/REP and PUB/SUB. With REQ/REP, sent messages are guaranteed to reach their destination, as the sender waits for a reply from the receiver, similar to \ac{tcp}'s acknowledgement signal. With PUB/SUB, messages are just sent out, regardless of the number and the status of possible recipients. Thus, if a message is missed by some receiving instance, there is no possibility for recovery.

Besides being more suitable in general due to above features, another reason for choosing \ac{zmq} is a Python module named ImageZMQ \autocite{bassJeffbassImagezmq2021} that is built on top of it, providing a \ac{api} specifically for image transmission. It allows either sending raw image data, being stored in \texttt{numpy} arrays, or compressing them before sending using the \ac{jpeg} \autocite{wallaceJPEGStillPicture1992} standard. In addition to image data, a string can be transmitted. Allowing transmission of image and remaining data in one message and circumventing cumbersome reassignment as needed in case of separate transmission, the logical consequence is to set up an encoding that structures data to be sent as a string.

Such a serialization can be accomplished by the \ac{json} \autocite{brayJavaScriptObjectNotation2017} format. Considering each string part of a ImageZMQ message as a \ac{json} object, the data to be sent is shown in \cref{lst::json_to_server}. Concrete data types correspond to the requirements of the tasks being explained in \cref{ch::task_identification}.

Concerning the inverted direction -- data transmission from server to client --, the amount of data is much smaller. Here, only flags for start/stop and manual control states as well as engine and steering parameters in case of activated manual control have to be sent. Despite the overhead of the \ac{json} format in comparison to binary encoding, it is used another time, since the resulting data volumes will be in the order of a few KBytes per second. Therefore, data is to be transmitted in packets as shown in \cref{lst::json_to_client}.


\subsection[Server-side Implementation]{Server-side Implementation $\dagger$} \label{ch::implementation_server}
This section splits up into two subsections. The former addresses the \ac{gui} design and usage, the latter one deals with the implementation of the server's logic.

\subsubsection{Application Design}
The window application is written in Python using Qt. Qt is a software framework, natively written in \CC, for accessing multiple aspects of modern computer systems. In order to use Qt with Python, the PyQt5 module provides a comprehensive set of Python bindings for Qt5, which is the fifth major release of the framework.

In addition to the \CC~libraries, the Qt framework also contains the Qt Designer to create \ac{gui}s and generate the required code. This feature is also supported by the Python module. Thus, the application's window and its elements can be accessed and controlled using Python.

The main window is separated into three blocks: showing the status log, settings for the camera stream and finally a block to control the vehicle. The \textit{Status log} output can be further customized through a tab in the navigation bar. It is possible to hide unwanted information from direct output. Additionally, all outputs can be written into a log file for later inspection. A colorized label below the log output shows the current connection status. With the \textit{Camera stream} block, one can open the stream in additional window, with or without recognized objects and/or lines drawn in. Eventually, in order to start the car or taking over manual control, the third and last block of the \ac{gui} is used, labelled with \textit{Control}. If a dangerous situation occurs or the vehicle shows an unwanted behavior, one can push the start/stop button to quickly halt the vehicle.

\begin{figure}[ht]
    \includegraphics[width=\textwidth]{Pictures/main_window.png}
    \caption[Main window of server application]{The main application window:
    On the left-hand side, the camera stream can be configured and manual control may be taken. The vehicle can be halted by pressing the stop button.
    The right-hand side is largely taken by the status window. Variable names are in bold.
    Via the menu bar, the connection may be started or stopped and the status log can be configured.}
    \label{fig:main_app_window}
\end{figure}

\subsubsection{Server-side Data Handling} \label{ch:server_data_handling}
The server has three main tasks: 
\begin{itemize}
    \item receiving images and calculated data via ImageZMQ
    \item display and filter the received data
    \item sending controlling commands at request of the user
\end{itemize} 

To receive data with ImageZMQ, an eponymous image-hub has to be created, using the host's IP address and a specified port. All data received through this hub is saved into a deque, preventing the garbage collector and the window from crashing. A deque object behaves like a \grqq double ended queue\grqq, allowing quick removal and insertions of objects in a fixed length \cite{python_deque}. Here, the deque's maximum length is set to 10. If the bounded length is exceeded and new items are added at one end of the container, the corresponding number of items are discarded from the opposite end. The hub is closed when the main thread terminates.

The \ac{jpeg} compressed images and the \ac{json} encoded messages are continuously received and processed. To (optionally) draw lines and bounding boxes, the \ac{jpeg} image is decompressed in a first step and finally converted into a QImage. The latter can be drawn to the \ac{gui}, using the previously mentioned deque buffer and a so-called slot-method, being invoked when a new image is received from the ImageZMQ socket.
\Cref{fig:main_app_window} shows the \textit{camera stream} window, containing detected lines and objects. The \ac{json} message is encoded to a dictionary, which is thread-safe due to the \ac{gil} (see \cref{ch::parallelization_approaches}) and can be used in other threads. A bulk of the dictionary contains information, which will be displayed in the log window. To improve readability, a syntax highlighter is applied to these parts. Parallel to the log window output, a logger writes all received messages into an output file for subsequent inspection.

In order to communicate back to the NVIDIA Jetson Nano, a \ac{tcp} connection is established as well. If one wishes to halt the vehicle or take manual control (via a game pad), the server propagates these instructions back to the client. 

General parameters for the network communication (e.g. the server's IP address), car-specific variables (e.g. maximum speed, control angle) and logging parameters may be set in a general config file.
