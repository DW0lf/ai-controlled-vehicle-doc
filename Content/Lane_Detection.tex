%!TEX root = ../report.tex
\section{Lane Detection} \label{ch::lane_detection}

Just like autonomously driving cars in real traffic, the presented vehicle should also be able to follow road lines automatically.
For this purpose, a stable lane detection algorithm must be developed, which returns the position of the road markings in such a way that another software part can take care of converting these positions into control instructions.
Here, blue painter's tape with a width of 2.5 cm is used as road marking.

In the following sections, the term \textit{lane} depicts the road markings, while \textit{line} denotes a straight line in geometric sense.
A lane is specified either by a single edge, or in the best case by both outer edges.

First, the analysis and pre-processing of images containing lanes is described.
Subsequent, the line detection process is explained.
Afterwards, the joining of lines is described, which became necessary due to the previous processing steps.
Finally, the implemented lane detection algorithm is tested on four test images of different scenes.

\subsection[Analyzing Images]{Analyzing Images $\ast$ $\ddagger$} \label{ch::analyzing_images}

Since the lane detection process is solely based on the images acquired by the camera and no other input data can be considered to support, analyzing of the images and working out their main properties is essential to detect lanes reliably. \Cref{fig:lanes_1} shows an example images captured by the camera in the chosen setting.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{Pictures/lane_detection/lanes_1.png}
    \caption[Example images showing lanes]{Example image showing lanes.}
    \label{fig:lanes_1}
\end{figure}

\subsubsection{Blurring} \label{ch::blurring}

Due to the limited lighting and the quality of the used camera sensor, acquired images tend to be noisy. One might e.g. notice the noise that occurs especially on the left side of \cref{fig:lanes_1}. While the blue lanes are easily distinguishable from wooden-like ground for humans, a computer vision system that operates on the basis of pixel-wise comparisons is presented with problems as a consequence of noise.

Gaussian Blurring is a method in which a transform originated from the Gaussian function is applied to every pixel of an image, blurring the image and reducing noise. The two-dimensional Gaussian function is given by

\begin{equation}
    G(x,y) = \frac{1}{2\pi\sigma^2}\cdot\exp{\left(-\frac{x^2+y^2}{2\sigma^2}\right)},
    \label{eq:2d_gauss}
\end{equation}

where $x$ and $y$ denote the horizontal respectively vertical distance from origin and $\sigma$ denotes the standard deviation of the Gaussian distribution \autocite{jaehneDigitaleBildverarbeitung2002}. 

Gaussian Blurring can be performed efficiently utilizing convolution with the values of the kernel forming a Gaussian distribution and the center of the kernel marking the origin. In order to be able to operate on integer values and achieve high performance, an approximation of the true Gaussian blur kernel is used in practice \autocite{jaehneDigitaleBildverarbeitung2002}.

\begin{equation}
    \boldsymbol{\mathrm{G}} = \begin{bmatrix}
        1 & 2 & 1 \\
        2 & 4 & 2 \\
        1 & 2 & 1 \\
       \end{bmatrix}
    \label{eq:2d_gauss_kernel}
\end{equation}

\Cref{eq:2d_gauss_kernel} shows such a Gaussian blur kernel with kernel size $3\times 3$ and $\sigma=0.8$\footnote{The parameter $\sigma$ is computed from the kernel size $n$ as $\sigma=0.3\cdot((n-1)\cdot 0.5 - 1) + 0.8$ here.}.

\subsubsection{\acs{roi} Cropping} \label{ch::roi_cropping}

Since lanes appear only in the lower part of the image, as shown in \cref{fig:lanes_1}, and the trajectory planning should only take lanes close to the car into consideration for steering, the image can be cropped after blurring to avoid performing the subsequent operations on the entire image.
Thus, only the lower two thirds of the image are used for further detection of lanes.
Next to this cut, the bottom pixel row as well as the leftmost and rightmost pixel columns are truncated from the image.

This is necessary due to the border effects caused by the prior blurring \cite{nischwitz_bildverarbeitung_2020}.
Since the kernel is moved over the image to be centered on each pixel, it overhangs the image in the edge areas. 
That is why, the image must be padded, in which case the value 0 is assigned to the attached pixels.
Consequently, the borders of the blurred image falsify the original data.
As a $3 \times 3$ kernel is utilized for blurring, only one pixel row / column at each border needs to be cropped.

\begin{figure}
    \centering
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/roi.png}
        \caption{}
        \label{fig:example_image_cropped}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/red.png}
        \caption{}
        \label{fig:example_image_cropped_red}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/green.png}
        \caption{}
        \label{fig:example_image_cropped_green}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/blue.png}
        \caption{}
        \label{fig:example_image_cropped_blue}
    \end{subfigure}
    \caption[Cropped example image]{Cropped \ac{roi}~of size $ 605 \times 209 $ for example image from \cref{fig:lanes_1}, (a) all RGB channels, (b) red channel, (c) green channel, (d) blue channel.}
    \label{fig:lanes_cropped}
\end{figure}

Eventually, an image of size $ 605 \times 209 $ is returned, as seen in \cref{fig:example_image_cropped} for the original input image \cref{fig:lanes_1}.

\subsubsection{Binarization} \label{ch::binarization}

Now, as the relevant part is extracted from the image, the properties of the lanes in the image can be investigated. 
After a unique property is found for the lanes, this can be used to binarize the image.

At first, the approach suggested by Tian \cite{deeppicar} is tested on the acquired images.
With this, the image is first converted to the HSV color space.
\Cref{fig:lanes_hsv} shows the cropped \ac{roi} converted to the HSV color space.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/hsv.png}
        \caption{}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/example_image_hue.png}
        \caption{}
    \end{subfigure}
    \caption[Example image in HSV color space]{Example image from \cref{fig:lanes_cropped} in HSV color space (a) all channels, (b) hue channel in gray scale. }
    \label{fig:lanes_hsv}
\end{figure}

The HSV color space represents a color by the \textbf{H}ue, \textbf{S}aturation and \textbf{V}alue.
The latter matches the maximal value of the RGB channels.
Next, the saturation $S$ is calculated from the RGB values by:
\begin{equation}
   S = 
   \begin{cases}
       \frac{max(R, G, B) - min(R, G, B)}{max(R, G, B)} &  V \neq 0 \\
       0 & otherwise
   \end{cases}
\end{equation}
The \textbf{H}ue component specifies the actual color by a degree value \cite{nischwitz_bildverarbeitung_2020}, as show in \cref{fig:hue_channel}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.3\textwidth]{Pictures/lane_detection/hue_channel.png}
    \caption[Hue channel of HSV color space]{Hue channel representation in HSV color space \cite{deeppicar}.}
    \label{fig:hue_channel}
\end{figure}

Therefore, this color model allows better differentiation between colors without considering their brightness, as required in this case to distinguish the lanes from the surrounding area.
To do this, only a limited range of values is allowed on the \textbf{H}ue channel.
Additionally, ranges for \textbf{S}aturation and \textbf{V}alue must be determined \cite{deeppicar}.

However, as one can recognize in \cref{fig:lanes_hsv} and as further investigations showed, the lanes in this case cannot be separated so well with this method, which is probably caused by the lighting conditions in the test area and the applied camera settings. 
Therefore, the approach was discarded for this use case.

Next, the RGB channels were examined individually. The separated channels are shown in \cref{fig:example_image_cropped_red,fig:example_image_cropped_green,fig:example_image_cropped_blue}. There, one can see that the blue lanes have nearly the same red value as the wood-like ground.
Additionally, the distribution on the red channel nearly match the background of the green channel. Hence, the lane segmentation can be performed by subtracting the red channel from the green channel.
To avoid negative values, a constant value is added to the green channel before subtraction. The result of this subtraction for the cropped example image is shown in \cref{fig:example_difference_rg}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/lane_detection/diff_rg.png}
    \caption[Difference image of green and red channel for example image]{Difference image of green and red channel for example image from \cref{fig:lanes_cropped}.}
    \label{fig:example_difference_rg}
\end{figure}

Thus, the difference of the green and the red color channel was determined as the unique feature of the road markings, as mentioned in the introduction of this section. Now, the result is to be binarized. Therefore, the average over the complete difference image is computed and multiplied with a constant factor, which was determined experimentally.
Finally, the thresholding by the computed value yields a binary image, as shown for the example image in \cref{fig:example_bin}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/lane_detection/bin.png}
    \caption[Binarized image of lanes]{Binarized image for difference image from \cref{fig:example_difference_rg}.}
    \label{fig:example_bin}
\end{figure}

In comparison to the first approach, this one does not require a color space transformation, but performs the subtraction of two color channels.

\subsubsection{Morphological Operations} \label{ch::morphological_operations}

Due to non-ideal road markings and white noise in the binarized image, the edges of the lanes are not that straight as required for the following processing steps.

Thus, morphological closing is applied to the image, consisting of two steps:
\begin{enumerate}
    \item \textbf{Dilation}: The kernel is moved over the image to be centered on each pixel, the reference point. If any image pixel in the neighborhood of the reference point on which the kernel is located and which has a value greater than zero also has a value greater than zero, the operation returns True.
    Thus, the reference point in the result image is set to the highest possible value, which in this case of 8-bit color depth is 255. 
    It equals a pixel-wise OR of the underlying image pixels and the kernel's values.
    Thereby, gaps between pixels are closed and edges are expanded.
    \item \textbf{Erosion}: As with dilation, the kernel is moved over the entire image, but the reference pixel in the resulting image is set to 255 only if the pixel-wise AND operation of the kernel and the corresponding image section returns True.
    Thereby, white noise can be removed \cite{opencv_morph}.
\end{enumerate}

For both operations, the following kernel is used:
\begin{equation}
    \boldsymbol{\mathrm{K}} = \begin{bmatrix}
        1   &   1   &   1   &   1   &   1 \\
        1   &   1   &   1   &   1   &   1 \\
        1   &   1   &   1   &   1   &   1 \\
        1   &   1   &   1   &   1   &   1 \\
        1   &   1   &   1   &   1   &   1 \\
       \end{bmatrix}
    \label{eq:kernel}
\end{equation}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/lane_detection/morph.png}
    \caption[Binarized, morphological adapted image of lanes]{Morphological closing with $ 5 \times 5$ rectangular kernel applied to binarized image shown in \cref{fig:example_bin}.}
    \label{fig:example_morph}
\end{figure}

As illustrated by \cref{fig:example_morph}, these operations close gaps in areas with mainly white pixels.
In comparison to the image from \cref{fig:example_bin}, this can especially be seen on the left lane.
Moreover, the lower part of the right, non-straight edge of the left lane is closed to a nearly straight line.
However, these operations also cause stairs-like effects, as they be can be recognized on the right side of the right lane.

As a result, a binarized image with nearly straight edges was generated and can be used for further processing steps. 

\subsection[Development of an Lane Detection Algorithm]{Development of an Lane Detection Algorithm $\ast$} \label{ch::lane_detection_development}

Now that the lanes could be separated from the rest of the image, their edges can be detected to find unique lines by which the road markings can be described. 
To do this, both the outer and inner edges should be detected to ensure that at least one line is found for each side.
For the following control it is necessary that the edges are described by a line given by a start pixel and an end pixel.

\subsubsection{Canny Edge Detection} \label{ch::canny_edge}

First, Canny Edge Detection is used to determine the edges of the separated road markings.
This requires the image to be in gray scale, which is satisfied by the input image due to prior binarization.

In the first step, another blurring with a $3 \times 3$ Gauss kernel is applied on the image.
Thereby, high frequency noise is reduced, because it could have a negative impact on the following steps.
In case of the binary input image, this means that the edges are smoothed.

Afterwards, the gradients are to be determined by the application of the Sobel operator, which is described more in detail in \cref{ch::traffic_sign_recognition}, in x and y direction \cite{nischwitz_bildverarbeitung_2020}.
Due to separate execution in x and y direction, these two images need to be combined to one.
Thus, the angle $A$ and absolute value $E$ of the final gradient can be calculated from the components $G_x$ and $G_y$ by \cite{opencv_canny}:

\begin{equation}
    \centering
    E(G_x, G_y) = \sqrt{G_x^2 + G_y^2}
\end{equation}
\begin{equation}
    A(G_x, G_y) = tan^{-1}(\frac{G_x}{G_y})
\end{equation}

Subsequently, non-maximum suppression is used for the gradient image to extract the actual gradient maxima, i.e. the edge of the lane.

Finally, a hysterese condition is applied to determine if a detected edge is really an edge or only an artifact, which does not belong to the actual edge. Here, for each pixel on the line, it is checked whether its value is between a minimum and maximum threshold or not.
If the condition is satisfied for at least one of them, the edge is accepted. Otherwise, the pixels in the result image belonging to this edge are set to 0 \cite{nischwitz_bildverarbeitung_2020}.
Thus, this step also removes noise from the image and a clean gradient image is returned.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/lane_detection/canny.png}
    \caption[Result of Canny Edge filter applied to image of lanes]{Result of Canny Edge filter applied on the image shown in \cref{fig:example_morph}.}
    \label{fig:example_canny}
\end{figure}

The result of Canny Edge Detection applied to the example image is shown in \cref{fig:example_canny}. One can see in this figure that not all the noise could be removed, but the edges of the lanes could be separated clearly. However, as mentioned in the introduction, not all pixels belonging to a line are searched for, so further processing steps have to follow to determine the coordinates of the points bounding the lines.

\subsubsection{Hough Line Transformation} \label{ch::hough_line}

The Hough Line Transformation is a commonly used method for detection of straight lines in binary images. Thus, it is also applicable on the result of Canny Edge Detection.

The basic idea of this transformation is the creation of straight lines from two relevant points and the subsequent determination of the frequency of all straight lines. In this case, the relevant points are the white pixels, representing the edges of the road markings.\\ The OpenCV function \texttt{HoughLinesP} implements the storage of the lines in polar coordinates $(r, \Theta)$, where the stored point corresponds to the intersection of the line with the unit circle scaled by $r$, i.e. the tangent to it.

This function allows to specify a distance resolution \texttt{rho}, concerning the intercept and angular resolution \texttt{theta} in radians concerning the slope in Cartesian coordinate system. Thereby, it is possible to group lines with a certain similarity before frequency analysis.

Finally, the frequency of all groups is determined and the groups with a given frequency \texttt{threshold} are returned, represented by start and end points. These points correspond to the two farthest points actually occurring in the image and being part of the line.

In comparison to OpenCV's function \texttt{HoughLines}, the aforementioned one is a more performant implementation due to excepting lines and ignoring appearances of them afterwards if they have reached a specific count \cite{opencv_bradski}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/lane_detection/hough.png}
    \caption[Result of Hough Transformation applied to Example image]{Result lines of Hough Lines Transformation applied on the image shown in \cref{fig:example_canny}.}
    \label{fig:example_hough}
\end{figure}

\Cref{fig:example_hough} presents the result of Hough Lines Transformation applied on the binary image from \cref{fig:example_canny}.

It is obvious, that lines with nearly the same slope and intercept either overlap each other or are distanced by some pixels, but are not merged to one. Thus, to obtain a minimum of lines specifying similar directions in similar areas, the line joining must be added as further processing step. \Cref{ch::line_joining_development} explains this more in detail.

\subsubsection{Offset Correction} \label{ch::offset_correction}

Since the relevant \ac{roi} was cropped out and used for further processing at the beginning, all coordinates are given relative to the origin of the cropped image, which does not correspond to that of the original image. That is why all determined coordinates have to be translated to relate to the initial origin.

The application of this translation finally results in the start and end points of lines, which were found in the image and should be road markings.

\subsection[Development of an Line Joining Algorithm]{Development of an Line Joining Algorithm $\ddagger$} \label{ch::line_joining_development}

In this section, the aforementioned algorithm for line joining is explained in detail. The algorithm can be divided into two parts, the first of which separates different classes of lines and the second of which joins subsets of lines inside their class, if class-specific criteria are fulfilled.

\subsubsection{Separation of Line Classes} \label{ch::seperate_line_classes}

First, there are three different classes of lines that are to be differentiated:

\begin{enumerate}
    \item Vertical lines that are located on the left half of the image
    \item Vertical lines that are located on the right side of the image
    \item Horizontal lines
\end{enumerate}

This specific classification originates from two main observations:

\begin{enumerate}
    \item Horizontal and vertical lines require different joining criteria.
    \item To join lines, every line has to be compared with every distinct line. Pre-joining separation of left and right located lines reduces the number of comparisons in total.
\end{enumerate}

To classify lines, their slope as well as the locations of their start and end points are evaluated. If the absolute value of the slope is below a certain threshold, the line is assumed to be horizontal, else vertical. If the latter applies, the line is classified left sided if its start and end point are located on the left side of the image. Analogously, it is classified right sided if its start and end point are located on the right side of the image. Last, an edge cases occurs, if at least one line crosses the vertical center axis of the image. Then, separation of left sided and right sided lines cannot be performed and the aforementioned optimization has to be omitted.

\subsubsection{Criteria for Line Joining} \label{ch::line_joining_criteria}

Two different lines should be connected if they are close to each other and have approximately the same slope. In the following, these two criteria are referred to as \textit{spatial proximity} and \textit{angular similarity}.

The determination of angular similarity is based on a comparison of two lines' slopes. If the difference of the slopes falls below a certain threshold, the corresponding lines are considered to be angularly similar, otherwise they are not.

As mentioned above, the fulfillment conditions of the spatial proximity criterion differ between horizontal lines and vertical lines. For horizontal lines, one can extend the two lines to the left and right image borders using the slope and then compute the average $y$ coordinate $\overline{y}$ of extended lines' start and end points. Here, $\overline{y}$ is equivalent to the $y$ coordinate of the intersection point of the extended line and the vertical center axis. If the difference between both lines' $\overline{y}$ is below a certain threshold, they are considered to be spatially proximal, otherwise they are not.

For vertical lines, this simple criterion respectively an analogue that utilizes $x$ coordinates is not sufficient, since slopes can vary to a higher extend. Instead, it is checked if the second line's start or end point is located in a rectangular area around the first line. To do so, a plane is spanned by the first line's direction vector $\boldsymbol{\mathrm{u}}$ and the corresponding perpendicular vector $\boldsymbol{\mathrm{v}}$. Note that the plane's origin is given by the start point $\boldsymbol{\mathrm{p^{(1)}_s}}$ of the first line. Then the start or end point $\boldsymbol{\mathrm{p^{(2)}}}$ of the second line is in the $\omega$ px broad rectangular area around the first line, if the following holds:

\begin{equation}
    \exists \lambda, \mu: -\frac{\omega}{\|\boldsymbol{\mathrm{u}}\|} \leq \lambda \leq 1 + \frac{\omega}{\|\boldsymbol{\mathrm{u}}\|}:  |\mu| \leq \frac{\omega}{\|\boldsymbol{\mathrm{v}}\|}: \lambda \cdot \boldsymbol{\mathrm{u}} + \mu \cdot \boldsymbol{\mathrm{v}} = \boldsymbol{\mathrm{p^{(2)}}} - \boldsymbol{\mathrm{p^{(1)}_s}}
    \label{eq:spatial_proximity_vert}
\end{equation}

\Cref{fig:spatial_proximity_vert} shows the described relations. In practice, the spatial proximity condition stated in \cref{eq:spatial_proximity_vert} can easily be verified or falsified by solving the $2 \times 2$ system of linear equations for $\lambda, \mu$ and subsequently checking their preconditions.

\begin{figure}
    \centering
    \includegraphics[width=0.6\textwidth]{Pictures/vertical_spatial_proximity.pdf}
    \caption[Schematic visualization of spatial proximity for vertical lines]{Schematic visualization of spatial proximity for vertical lines. Here, the line that is defined by the points $p_s^{(2)}$ and $p_e^{(2)}$ is considered to be spatially proximal w.r.t the line that is limited by $p_s^{(1)}$ and $p_e^{(1)}$, since $p_s^{(2)}$ is located in the $\omega$ px broad rectangle (green) around the latter.}
    \label{fig:spatial_proximity_vert}
\end{figure}

\subsubsection{Line Joining} \label{ch::line_joining}

As different classes of lines are separated from each other and joining conditions are defined, the actual joining process can start. Joining is basically performed per class; however, if left and right sided vertical lines could not be separated, the set of all vertical lines is treated as a cohesive class instead.

Theoretically, each line of a given class must be compared with every other line belonging to that class, checking the joining criteria. Eventually, one can join all those line pairs which fulfill the conditions.

In practice, it is more efficient to perform joining immediately after detecting condition fulfillment, reducing the number of lines and hence the number of remaining line comparisons. In consequence, the time complexity of the developed algorithm varies, dependent on the proportion of joinable lines. If no lines can be joined, it has a worst case complexity of $\mathcal{O}(n^2)$, since all lines are compared unsuccessfully with each other. On the other hand, if all lines can be joined to a single one, best case complexity $\mathcal{O}(n)$ is achieved.

If two lines being defined by their start and end points $p_s^{(1)}, p_e^{(1)}, p_s^{(2)}, p_e^{(2)}$ fulfill the joining criteria of angular similarity and spatial proximity, they are joined such that the resulting line has maximal length. To do so, the reasonable combinations of lines' start and end points are evaluated in terms of their distance:

\begin{equation}
    \argmax_{(p_s, p_e) \in P}{\| p_s - p_e \|}\quad \mathrm{with} \quad P=\{(p_s^{(1)}, p_e^{(1)}), (p_s^{(2)}, p_e^{(2)}), (p_s^{(1)}, p_e^{(2)}), (p_s^{(2)}, p_e^{(1)})\}.
\end{equation}

As start points always have a lower $y$ value than end points, the combinations $(p_s^{(1)}, p_s^{(2)})$ and $(p_e^{(1)}, p_e^{(2)})$ can be omitted here.

\subsection[Test]{Test $\ast$} \label{ch::lane_detection_test}

Last, the implemented lane detection is tested on example images, which are shown in \cref{fig:test_images}. 

\subsubsection{Test of Lane Extraction}

\begin{figure}
    \centering
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/lanes_1.png}
        \caption{}
        \label{fig:test_image_1}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/line2.png}
        \caption{}
        \label{fig:test_image_2}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/curve1.png}
        \caption{}
        \label{fig:test_image_3}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/horizontal2.png}
        \caption{}
        \label{fig:test_image_4}
    \end{subfigure}
    \caption[Test images for lane detection]{Test image for lane detection algorithm. (a) slight curve, (b) straight track, (c) sharp curve, (d) stop / horizontal lane}
    \label{fig:test_images}
\end{figure}


\begin{figure}[h]
    \centering
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/line1_lines.png}
        \caption{}
        \label{fig:test_image_1_lines}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/line2_lines.png}
        \caption{}
        \label{fig:test_image_2_lines}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/curve1_lines.png}
        \caption{}
        \label{fig:test_image_3_lines}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/horizontal_lines.png}
        \caption{}
        \label{fig:test_image_4_lines}
    \end{subfigure}
    \caption[Extracted lines in test images]{Result of line extraction applied on images from \cref{fig:test_images}}
    \label{fig:test_images_lines}
\end{figure}

The application of the lane extraction, without the subsequent joining, results in the images shown in \cref{fig:test_images_lines}. 

As one can see there, most of the lanes are reliably detected up to a certain distance. Even in \cref{fig:test_image_3_lines}, which shows a sharp curve, at least the inner edge of the road marking is detected, allowing reliable control planning. Only the detection of the horizontal lane causes problems, which could be due to the frontal viewing direction of the camera. However, this problem is solved by subsequent joining, as shown in \cref{fig:test_images_bundle}. 

\subsubsection{Test of Line Joining}
\begin{figure}[h]
    \centering
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/line1_bundle.png}
        \caption{}
        \label{fig:test_image_1_bundle}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/line2_bundle.png}
        \caption{}
        \label{fig:test_image_2_bundle}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/curve1_bundle.png}
        \caption{}
        \label{fig:test_image_3_bundle}
    \end{subfigure}
    \begin{subfigure}{0.4\textwidth}
        \includegraphics[width=\textwidth]{Pictures/lane_detection/test/horizontal_bundle.png}
        \caption{}
        \label{fig:test_image_4_bundle}
    \end{subfigure}
    \caption[Joined lines in test images]{Result of line joining applied on images from \cref{fig:test_images_lines}}
    \label{fig:test_images_bundle}
\end{figure}

Joining significantly reduces the number of lanes found, without losing relevant directional information.\\
However, distant horizontal lines, such as the leading and trailing edges of the stop line in \cref{fig:test_image_4_lines}, may be joined into one line. Therefore, depending on the width of the road marking, it is necessary to find a balance for the adjustable parameters of the joining process.

\subsubsection{Optimization of the Line Joining Implementation}

Due to a worst case complexity of $\mathcal{O}(n^2)$, which is caused by two nested for-loops, the duration of the sole line joining extends into the two-digit millisecond range, as shown in \cref{fig:numba_benchmark}.\\ 
Thus, additional optimizations are required to avoid the Lane Detection process blocking the other processes. Therefore, the aforementioned Python library \texttt{numba} is used to precompile the line joining code. \\
Consequently, a speedup of the line joining up to a factor of 26  -- for scenarios with few lines -- is achieved.
In addition, the standard deviation of the precompiled code, independent of the amount lines to consider, is reduced and thereby the line joining process becomes more predictable.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/numba_benchmark.pdf}
    \caption[Comparison of duration of line joining code in pure Python and precompiled with \texttt{numba}]{Comparison of durations of line joining in pure Python (blue bars) and precompiled with \texttt{numba} (red bars), the error bars denote the standard deviation. Scenarios correspond to images in \cref{fig:test_images_lines}}
    \label{fig:numba_benchmark}
\end{figure}

All in all, a stable algorithm for detection of blue road markings in a specific environment was developed, identifying lanes that can be translated to control signals.
