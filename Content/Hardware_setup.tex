%!TEX root = ../report.tex
\subsection[Hardware Assembly]{Hardware Assembly $\ast$ $\dagger$} \label{ch:hardware_assembly}
\Cref{fig:hardware_wiring_layout} shows the general wiring layout. There are two power sources, the first of which powers the \ac{soc} as well as its peripherals and the second powers the car and its components. A more detailed explanation is given in \cref{ch:power_distribution}.

\begin{figure}[ht]
    \centering
    \includegraphics[scale=0.5]{Pictures/layout_general.png}
    \caption[Hardware wiring layout]{Hardware wiring layout.
        Red and black wires deliver voltage.
        Yellow wires are for analog signals and drive the motor and the steering servo.
        The medium thick green wire connects the NVIDIA Jetson Nano with the Servo board via \ac{i2c} bus.
        The thick black wire connects to the camera via CSI-2 protocol.}
    \label{fig:hardware_wiring_layout}
\end{figure}

There is a switch on the upper side of the \acs{esc}, which controls the lower block, consisting of the \acs{esc}, the servo driver and the servo motor. The NVIDIA Jetson Nano is connected to the servo driver via \acs{i2c} bus.

Three ultrasonic sensors are connected to the NVIDIA Jetson Nano via \acs{gpio} pins, with the required resistors being soldered on an intermediate board. For clarity, these have been omitted from \cref{fig:hardware_wiring_layout}.

The camera uses, as the Raspberry Pi too, the \ac{csi2} protocol from \ac{mipi}. This protocol is primarily used for point-to-point image and video transmission\cite{mipi_csi}.

The car is powered by a DC motor, which is mounted on the rear axle. It's an older model with brushes. Since the car was originally designed to drive on uneven terrain, the engine is additionally equipped with a differential.

An analog servo motor steers the front wheels. The mechanism is simple and somewhat loose. To facilitate stable steering, this must be modified.
%Every participant in an $I^2 C$ network has a unique address. In case of the servo board, this is set by means of closing on solder joints. Since the board will be the only slave in the network, it can keep the address 0 and no soldering is required.

\subsubsection{\acs{pwm} Driver} \label{ch:pwm_driver}
Connecting the \ac{esc}, which was part of the used chassis, directly with the NVIDIA Jetson Nano turned out to be a problem: While the \ac{esc} obtains energy from the \ac{nimh} battery, the NVIDIA Jetson Nano consumes power from the \ac{lipo} battery.
Investigations show that the batteries used for this project provide distinct ground potentials.
\Cref{fig:esc_forward} shows the voltage profile for forward signal send by the NVIDIA Jetson Nano.
This profile was measured at the ESC and shows a voltage offset of approximately 0.3~V caused by the distinct ground potentials of the separate circuits.
Because of this offset, the \texttt{HIGH} voltage is around 3.6~V instead of 3.3~V (observed using the original receiver).
Since the ESC determines the output voltage and thereby the speed of the brushed motor by the cycle time and the size of the input voltage, the output to the motor is bigger for a higher input voltage \cite{esc}.
However, the PWM pins of the NVIDIA Jetson Nano supply too low current for some use cases, as can be read in forums \cite{jetson_gpio}.

\begin{figure}
    \centering
    \includegraphics[width=0.95\linewidth]{./Pictures/esc_forward.pdf}
    \caption[Voltage curve for forward signal send to the \ac{esc} in initial setup]{Voltage curve
        (measured at \ac{esc}) for forward signal send directly from NVIDIA Jetson Nano to the \ac{esc}.}
    \label{fig:esc_forward}
\end{figure}

This is why an additional component, the Adafruit PCA9685 PWM driver, is used to manage all \ac{pwm} outputs.
This model allows controlling up to 16 PWM outputs per 12bit resolution.
It is connected to the NVIDIA Jetson Nano via the $I^2C$ bus \cite{i2c} and has its own clock.
Furthermore, additional connections for input power supply at the PWM driver -- that can be connected to another device than the sender of the signals -- make physically separated power circuits possible \cite{adafruit}.
Thus, this component delivers a more stable and correct PWM signal to the \ac{esc} by using the \acs{nimh} battery as power supply.

\subsubsection{Servo motor} \label{ch:servo_motor}
A servo motor is an electromechanical device which rotates a drive shaft into a specific position.
The angle of rotation depends on the input signal, more precisely on the signal's pulse width \cite{salah}.

\begin{figure}
    \centering
    \includegraphics[width=0.25\linewidth]{./Pictures/servo_master_s4020.jpg}
    \caption[Servo motor for steering]{Servo motor \textit{Master S4020} used
        for steering \cite{servo_specs}.}
    \label{fig:servo}
\end{figure}

Consequently, the analog servo motor \cite{servo_specs} that controls the steering of the presented chassis -- shown in figure \cref{fig:servo} -- is connected to the Adafruit \ac{pwm} driver as well.

\subsubsection{Ultrasonic sensor} \label{ch:setup_ultrasonic_sensor}
Here, three HC-SR04 sensors are used: one at the front, one on the right and one at the rear of the car.
This model emits eight cycles of $40$~kHz signal.
Afterwards, the sensor waits for the detection of reflected signals and when received them, sets its \texttt{ECHO} pin too high, as long as it waited for the signal \cite{hcsr04}.
Due to that, the distance between the sensor and the detected object is calculated by:
\[d = \frac{t \cdot v_S}{2}\]
where $d$ determines the distance, $t$ denotes time for high output and $v_S$ corresponds to the sonic speed (approx. $343 \frac{m}{s}$).

\begin{figure}
    \centering
    \includegraphics[width=0.7\linewidth]{./Pictures/ultrasonic_sensor.jpg}
    \caption[Connection of ultrasonic sensor to NVIDIA Jetson Nano]{Circuit
        diagram for connection of ultrasonic sensor HC-SR04 to NVIDIA Jetson Nano's
        GPIO (\texttt{IN} denotes a digital input, \texttt{OUT} denotes a digital
        output)}
    \label{fig:ultrasonic_sensor}
\end{figure}

Figure~\ref{fig:ultrasonic_sensor} provides the circuit diagram for the connection of this ultrasonic sensor model to the NVIDIA Jetson Nano.
There, one can see a voltage divider that lowers the voltage from 5~V to 3.3~V to prevent damage at the \ac{gpio} \cite{pi_us}.

\subsubsection{Camera} \label{ch:camera}
As investigated in another study of building an autonomous driving car based on an embedded system \cite{deeppicar}, a wide angle camera is more appropriate to the application of lane detection.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.35\linewidth]{./Pictures/imx219-170-camera-3.jpg}
    \caption[Waveshare IMX219-170 camera]{Waveshare IMX219-170 camera module
        \cite{imx219}.}
    \label{fig:cam}
\end{figure}

Thus, the Waveshare IMX219-170\cite{imx219} is chosen for this project. It has a $170^\circ$ angle of view, a resolution of 8~MP and can be connected to NVIDIA Jetson Nano's \ac{mipi} CSI-2 port. \Cref{fig:cam} shows this camera model.

\subsubsection{WiFi Dongle}
The NVIDIA Jetson Nano itself has no \ac{wifi} card installed but supports various \ac{usb} dongles to obtain wireless capabilities.
At the beginning of this work, a wireless \ac{usb} adapter of the type Edimax EW-7811Un was available. This adapter complies with the wireless IEEE 802.11b/g/n standards and delivers transmission rates up to 150Mbps.
These data rates, specified in the data sheet, initially seemed sufficient, but practical tests showed that the actual data rates were far below the specified ones. The test results can be seen in \cref{app:wlan-dongle-iperf} and were computed with iPerf3\footnote{\url{https://iperf.fr/}}, a tool for active measurements of bandwidths on IP networks.
Besides the insufficient test results, the communication between \ac{soc} and PC turned out to be too unstable and slow.

These circumstances have been eliminated by replacing the initial dongle by a \ac{wlan} ac capable \ac{usb} dongle\footnote{Lemorele WiFi Adapter 1200Mbit/s}. The IEEE 802.11ac standard works on the 5 GHz band and a single-link throughput of at least 500Mbps \autocite{colbach_wifi_nodate}. In consequence, a much faster and more resilient connection is achieved, as can also be seen in \cref{app:wlan-dongle-iperf}.

\subsubsection{Power distribution} \label{ch:power_distribution}
As mentioned in \cref{ch:pwm_driver}, there are two distinct power circuits on the car's platform.

On the one hand, there is the \acs{nimh} battery which is part of the original model car.
It powers the brushed motor, the \ac{esc}, the servo driver and the servo motor at a voltage of 7.2~V (respectively 3.3~V to all components wired to the \ac{esc}).

On the other hand, a \acs{lipo} battery is mounted on top of the chassis and powers the NVIDIA Jetson Nano and all its connected components.
This \ac{soc} takes at least 5~W in low energy mode, but with reduced \ac{cpu} and \ac{gpu} frequency, and 20W at the most \cite{jetson_guide}.

Because autonomous driving requires the best performance of the SoC, it is powered with 20~W.
In order to provide this amount of power and keep it as constant as possible, a step down converter \cite{step_down} is utilized.
It has two separate controllers for voltage and current.
These are set to 4~A current and 5~V voltage.
With decreasing battery level, the voltage declines.
This is, however, not a problem until the voltage falls below 4.25~V, which is the minimal voltage required by the NVIDIA Jetson Nano.

This separation of the power circuits optimizes the power supply to the NVIDIA Jetson Nano, because the other power intensive components are connected to the other circuit.
Eventually, a more stable behavior of the \ac{soc} can be expected.

\subsubsection{Design of the modular chassis} \label{ch:design_modular_chassis}
The regular chassis is too small to fit all components. Especially the footprint of the Jetson poses a problem. Therefore, the decision was made to print a superstructure which is tailored to the installed parts.
\Cref{fig:chassis_superstructure_upper_right} shows this structure from an upper right perspective. Additional perspectives are included in \cref{app:car-chassis-superstructure}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth, keepaspectratio]{Pictures/car_equipped/right.png}
    \caption[Chassis superstructure model]{3D-model of the printed superstructure, individual parts are color-coded
        (Gray: Platform for the NVIDIA Jetson Nano; Orange: Ultrasonic sensor clamps; Blue: General platform for battery, step-down converter and mount points; Red: Camera mount).}
    \label{fig:chassis_superstructure_upper_right}
\end{figure}

The blue part of the superstructure connects to the car's chassis. Thus, holes are embedded in it, with which the structure is aligned and fixed. Here, the two parallel support walls are used to keep the \ac{lipo} battery, which powers the NVIDIA Jetson Nano, in its position.
Between the camera mount and the first support wall, the step-down converter is bolted. Because the battery's weight is much higher than the other components, it was placed next to the rear axle. Therefore, the car can steer stably and the battery lies on a larger surface.

Since the differential is located above the rear axle, the NVIDIA Jetson Nano must be placed elevated above it.
The four protruding platforms lift the SoC slightly over the platform to ensure that it is also cooled from the bottom. The NVIDIA Jetson Nano is bolted on these platforms as well.

With the three orange clamps, the ultrasonic sensors are mounted to the superstructure. It was not necessary to place a fourth one on the right-hand side, since no obstacles are to be expected from the right that are not detected by the front sensor. Nevertheless, it would be technically possible to install the missing one at a later time.

The camera should be in an elevated position above the vehicle, which is why its structure meets this requirement. As depicted in \cref{app:car-chassis-superstructure}, camera's attachment has a score and two screw holes to align the camera. The cable runs over the converter and the battery.

Finally, the vehicle with all components installed is shown in \cref{app:car-fully-equipped}.
