%!TEX root = ../report.tex
\chapter{Methodology} \label{ch::methods_procedures}

This chapter deals with the methods that were used to process the tasks defined in \cref{ch:tasks}. Since this work is a practical computer science project, the chapter largely relates to used software. It is structured as follows:

\begin{itemize}
    \item In \cref{ch::programming_language}, the used programming language is discussed.
    \item In \cref{ch::add_used_software}, additionally utilized programs, frameworks, libraries, etc. are presented. 
    \item In \cref{ch::dev_workflow}, the development workflow is explained.
\end{itemize}

\section{Programming Language} \label{ch::programming_language}
Because of wide usage in the scientific domain and a powerful syntax, Python is the language chosen for this project.
Another advantage of this interpreter language is the platform independence, which allows development and testing on distinct platforms.
Moreover, Python is capable of rapid prototyping and thereby enables fast implementation of complex tasks while supporting different programming concepts, like object-oriented and functional \cite{lutz_python}.
As stated in \cref{ch:introduction}, there are other projects that address the development of an \ac{ai}-controlled vehicle using Python as programming language.
This circumstance is due to Python's ecosystem, which contains various libraries for all kind of applications, e.g. PyTorch for machine learning, OpenCV bindings for image processing and multiple libraries for I/O controlling.


\section{Additional Software} \label{ch::add_used_software}

The following tables contain additional used software along with the version numbers.
\Cref{tab::jetson_software_packages} lists software that needs to be installed on the NVIDIA Jetson Nano.
\Cref{tab::pc_software_packages}  does so for software required on the remote computer which is used for development and running the remote application.

All installed Python packages for NVIDIA Jetson Nano and the remote are gathered in \cref{tab::python_packages}. Some Python modules are needed on both the NVIDIA Jetson Nano and the remote computer, e.g. \texttt{ImageZmq} for image transmission over the network.

\begin{table}[ht]
    \begin{tabularx}{\textwidth}{|>{\hsize=1.3\hsize}X|>{\hsize=0.4\hsize}X||>{\hsize=1.3\hsize}X|}
        \hline
        \textbf{Name of the Software} & \textbf{Version} & \textbf{Usage} \\\hline \hline
        Jetpack                      & 4.5                     & Operating System, based on Ubuntu\\\hline
        CUDA                         & 4.5.0                   & Interface technology to run computations on \ac{gpu}\\\hline
        CMake                        & 3.10.2                  & Control software compilation\\\hline
        TensorRT                     & 7.1.3.0                 & NVIDIAs \ac{sdk} for deep learning inference\\\hline
        OpenSSH                      & 7.6p1                   & Premier connectivity tool for remote login with the SSH protocol\\\hline
        OpenSSL                      & 1.0.2                   & Toolkit for \ac{tls} and \ac{ssl} protocols\\\hline
        OpenCV                       & 4.5.0                   & Computer vision and machine learning software library\\\hline
        Python                       & 3.6.9                   & Programming language\\\hline
        LLVM                         & 9.0.1                   & Collection of modular and reusable compiler and toolchain technologies\\\hline
        llvmlite                     & 0.33.0                  & LLVM Python binding for writing JIT compilers\\\hline
        Threading Building Blocks (TBB)& 44 u4                   & Library to write parallel \CC~programs\\\hline
    \end{tabularx}
    \caption[Software versions on the NVIDIA Jetson Nano]{Software versions on the NVIDIA Jetson Nano.}
    \label{tab::jetson_software_packages}
\end{table}

\begin{table}
    \begin{tabularx}{\textwidth}{|>{\hsize=1.15\hsize}X|>{\hsize=0.7\hsize}X||>{\hsize=1.15\hsize}X|}
        \hline
        \textbf{Name of the Software} & \textbf{Version number} & \textbf{Usage}\\\hline \hline
        VS Code                      & 1.55.2 & \ac{ide}\\\hline
        Remote-SSH                   & 0.65.4 & Plugin to connect VS Code via SSH to a remote host\\\hline
        Python-Extension             & 2021.4.765268190 & VS Code's Python extension \\\hline
        \hline % end of VS Code and it's extensions
        OpenCV                       & 4.5.1 & Computer vision and machine learning software library \\\hline
        Qt                           & 5.9.7 & Software framework for \ac{gui}-development \\\hline
        Python                       & 3.8.5 & Programming Language \\\hline
    \end{tabularx}
    \caption[Software versions on the remote computer]{Software versions on the remote computer.}
    \label{tab::pc_software_packages}
\end{table}

\begin{table}[ht]
    \begin{tabularx}{\textwidth}{|>{\hsize=1.4\hsize}X||>{\hsize=0.6\hsize}X|>{\hsize=0.6\hsize}X|>{\hsize=1.4\hsize}X|}
        \hline
        \textbf{Name of the Python package}  & \textbf{Version (remote)} & \textbf{Version (Jetson)} & \textbf{Usage} \\\hline \hline
        Adafruit-Blinka                  & & 5.13.1 & (*)\\\hline
        Adafruit-CircuitPython-BusDevice & & 5.0.4 & (*)\\\hline
        Adafruit-CircuitPython-Motor     & & 3.2.5 & (*)\\\hline
        Adafruit-CircuitPython-PCA9686   & & 3.3.3 & (*)\\\hline
        Adafruit-CircuitPython-Register  & & 1.9.3 & (*)\\\hline
        Adafruit-CircuitPython-ServoKit  & & 1.3.1 & (*)\\\hline
        Adafruit-PlatformDetect          & & 2.28.0 & (*)\\\hline
        Adafruit-PureIO                  & & 1.1.8 & (*)\\\hline
        ImageZmq                         & 1.1.1 & 1.1.1 & Image transmission via network\\\hline
        Jetson.GPIO                      & & 2.0.15 & Handling of \acs{gpio} pins\\\hline
        Matplotlib                       & 3.3.4 & & Creation of plots\\\hline
        Netifaces                        & & 0.10.9 & Fetching the used IPv4 address\\\hline
        Numpy                            & 1.20.1 & 1.19.5 & Linear algebra\\\hline
        Numpy-Ringbuffer                 & & 0.2.1 & Ringbuffer data structure for Numpy arrays\\\hline
        Numba                            & & 0.50.1 & Just-in-time-compiler for Python\\\hline
        Pandas                           & 1.2.2 & & Data analysis and handling\\\hline
        PyQt5                            & 5.15.2 & & Python bindings for Qt (\acs{gui} framework)\\\hline
        PyYAML                           & 5.4.1 & 3.12 & Handling of YAML files\\\hline
        PyZmq                            & 22.0.3 & 21.0.2 & Python bindings for ZMQ (message protocol)\\\hline
        SciPy                            & 1.6.2 & 1.5.0 & Numerical algorithms\\\hline
        torch                            & 1.8.1 & 1.8.0 & Machine learning\\\hline
        torch2trt                        & & 0.2.0 & TensorRT optimization for PyTorch models\\\hline
        torchvision                      & 0.9.1 & 0.9.0 & Computer vision extensions for PyTorch\\\hline
    \end{tabularx}
    \caption[Python package versions on the remote computer and the NVIDIA Jetson Nano]{Python package versions on the remote computer and the NVIDIA Jetson Nano.\\(*): All packages prefixed with \texttt{Adafruit-} are needed for controlling microcontrollers, servos and motors.}
    \label{tab::python_packages}
\end{table}

\subsection{Precompiling Python Code with \texttt{numba}}
To speed up built-in Python functions and a subset of \texttt{numpy} operations, one can use the \textit{Just-in-time} compiler \texttt{numba}. The LLVM compiler structure is used to generate optimized machine code, which results in execution times that are comparable to \CC~or C code \cite{grover_speed_2019}. 
Currently, \texttt{numba} is limited to the functional programming approach, but supports the object orientation experimentally.
However, to achieve the best possible result and avoid bugs concerning the experimental nature of the later one, here only standalone functions are precompiled using \texttt{numba}.
Thus, to attach these functions to object-oriented structures additional wrapper-methods are required.

Since the NVIDIA Jetson Nano is based on the AArch64 architecture, it is not possible to simply install \texttt{numba} outside a Conda environment. Unfortunately, it is not possible to use such an environment, as the TorchRT tool is shipped with the Jetson operating system Jetpack. Therefore, it is tied to the system wide Python installation. So \texttt{numba} and all dependencies (llvm, llvmlite) were built from source, being a challenging venture due to the large number of cross-dependencies and architecture incompatibilities. 

\subsection{Additional self-compiled software}
OpenCV is an open-source library which bundles a lot of algorithms related to computer vision and machine learning. The \texttt{opencv-python} library is the contiguous Python-API, delivering not only language support, but also providing access to \texttt{numpy} containers\footnote{All OpenCV array structures are converted to \texttt{numpy} arrays and vise versa.} and functions. These wrapper functions import the computationally intensive \CC~functions as Python modules, making the code faster in comparison to Python-only adaptions \cite{howse_learning_nodate}.
In order to use \texttt{opencv-python}, the underlying OpenCV libraries have to be built from source. 

The \texttt{torch} library enables tensor computation with strong \ac{gpu} acceleration and is used for deep learning applications. Like the OpenCV library, the \texttt{torch} library has to be built from source, as no Conda environment can be used.
\textit{PyTorch} is a package for Python programs, which  bundles the \texttt{torch} library with \texttt{multiprocessing} units and -- like \texttt{numba} does -- a just-in-time compiler \cite{antiga_deep_2020}.

\section{Development Workflow} \label{ch::dev_workflow}

As mentioned in \cref{tab::pc_software_packages}, Microsoft Visual Studio Code is used on the remote computer for development.

Besides other language-specific extensions, the \textit{Visual Studio Code Remote - SSH} extension has proven to be very useful for programming directly on the NVIDIA Jetson Nano:
It opens an SSH tunnel for communication between the local Visual Studio Code instance and the Visual Studio Code Server that is installed on the NVIDIA Jetson Nano.
This setup allows source code editing, debugging and process management on the SoC via the local Visual Studio Code on the remote computer and thereby takes away the work of manual file transfer \cite{code_ssh}.

However, this workflow proved to be disadvantageous in terms of memory, especially after embedding traffic sign classification, as it entails a lot of overhead.


