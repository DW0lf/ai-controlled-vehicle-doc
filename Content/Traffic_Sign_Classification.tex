%!TEX root = ../report.tex
\section{Traffic Sign Classification} \label{ch::traffic_sign_classification}

This section deals with the classification of traffic sign candidates that were previously recognized using the algorithm presented in \cref{ch::traffic_sign_recognition}. Classification is based on machine learning, in detail a deep neural network is to be trained on a prepared dataset and subsequently performing inference on the incoming real application data. The rest of this section is structured as follows:

\begin{itemize}
    \item In \cref{ch::nn_dataset}, the training and test datasets are created.
    \item In \cref{ch::nn_architecture}, the used neural network architecture is presented.
    \item In \cref{ch::nn_training}, the neural network is trained.
    \item In \cref{ch::nn_acceleration}, the trained neural network is accelerated and ported to the NVIDIA Jetson Nano.
    \item In \cref{ch::nn_inference}, a final inference test in the real application environment is performed and evaluated.
\end{itemize}

\subsection[Creation of Training and Test Datasets]{Creation of Training and Test Datasets $\ddagger$} \label{ch::nn_dataset}

As being part of supervised learning algorithms, a neural network has to be trained with a training dataset before it can actually be used for e.g. classification. Moreover, a test dataset has to be created, which is withheld during training and used to measure the performance\footnote{The term performance here does not mean the computational speed, but the accuracy of the model.} of the model after training. Only using the training dataset for performance measurements carries the risk of overfitting, i.e. the model is correct w.r.t. the training data, but fails generalizing on unseen data \autocite{Goodfellow-et-al-2016}. Both the training and the test dataset should be representative for the data the model is confronted with during later application. 

As mentioned in \cref{ch::traffic_sign_candidates}, three different traffic signs as well as the three states of a traffic light are to be differentiated. These six entities are considered as distinct classes in the modeled classification task.

The complete training and test dataset used for model training and evaluation was handcrafted, meaning the images were manually captured using the traffic sign recognition algorithm presented in \cref{ch::traffic_sign_recognition} and eventually labeled. Since the turn left and the turn right signs are symmetrical, images were only taken for the first one and horizontally flipped to create data for the second one. All images were scaled to $32 \times 32$ px and normalized w.r.t. the whole dataset. \Cref{tab::dataset_class_distribution} shows the final absolute and relative image distributions across classes. Here, 80 percent account for the training data and 20 percent for the test data. Last, \cref{fig::dataset_examples} shows an excerpt from the unnormalized datasets.

\begin{table}[ht]
    \begin{tabularx}{\textwidth}{|X||X|X|}
        \hline
        \textbf{Class}          & \textbf{Absolute distribution} & \textbf{Relative distribution} \\\hline
        \hline
        Stop sign               & 211                & 18.48 \%          \\\hline
        Turn left sign          & 179                & 15.67 \%          \\\hline
        Turn right sign         & 179                & 15.67 \%          \\\hline
        Red traffic light       & 194                & 16.99 \%          \\\hline
        Yellow traffic light    & 199                & 17.43 \%          \\\hline
        Green traffic light     & 180                & 15.76 \%          \\\hline
        \hline
        Total                   & 1142               & 100.00 \%         \\\hline
    \end{tabularx}
    \caption[Class distribution for the training and test datasets (combined)]{Class distribution for the training and test datasets (combined).}
    \label{tab::dataset_class_distribution}
\end{table}

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/training_dataset_img_matrix.png}
    \caption[Excerpt from the training and test dataset]{Excerpt from the training and test dataset. Per column, four examples of the corresponding class are shown. In favor of image clarity, data is not normalized here.}
    \label{fig::dataset_examples}
\end{figure}


\subsection[Neural Network Architecture]{Neural Network Architecture $\ddagger$} \label{ch::nn_architecture}

\subsubsection{Fundamentals}

A neural network is a highly parameterized mathematical model $\hat{f}$ that is to approximate a certain, mostly non-linear function $f$, whose input $\boldsymbol{\mathrm{x}}$ and output $\boldsymbol{\mathrm{y}}$ are arbitrarily high-dimensional. Using categorical data as output $\boldsymbol{\mathrm{y}}$, a neural network can be used as a classifier \autocite{Goodfellow-et-al-2016}. In 1989, \citeauthor{cybenkoApproximationSuperpositionsSigmoidal1989} proved for the first time that neural networks are able to represent any continuous function \autocite{cybenkoApproximationSuperpositionsSigmoidal1989}. One reason for this capability is their high number of parameters $\boldsymbol{\mathrm{\theta}}$. These parameters are gradually adjusted in the training phase using correct input-output data pairs from the training dataset.

Feedforward neural networks, also called multilayer perceptrons, are a type of neural networks whose computational flow is straightforward, i.e. containing no cycles or feedback loops. Internally, they are built up from layers, where the first one is the input layer, the last one is the output layer and those in between are called hidden layers. A layer typically consists of linear mathematical operations, being followed by a non-linear activation function, which only make neural networks capable of approximating non-linear functions. An exception is the input layer, which simply passes the input data to the following layer. The two mostly used types of layers are fully connected layers and convolutional layers \autocite{Goodfellow-et-al-2016}.

Fully connected layers on the one hand multiply the vectorial input data $\boldsymbol{\mathrm{x}}$ by a parameter matrix $\boldsymbol{\mathrm{W}}$, being followed by the addition of a bias vector $\boldsymbol{\mathrm{b}}$:

\begin{equation}
    \boldsymbol{\mathrm{z}} = \boldsymbol{\mathrm{W}} \cdot \boldsymbol{\mathrm{x}} + \boldsymbol{\mathrm{b}}
    \label{eq:fully_connected_layer}
\end{equation}

Convolutional layers on the other hand convolve the input data with a kernel to produce a so-called feature map. They are often used to process two-dimensional image data in the early stages of a neural network. Here, they can be interpreted as a kind of edge detectors. As two-dimensional image data is frequently composed by several channels (e.g. RGB), both the input data and the kernel have to be three-dimensional tensors. Moreover, a convolutional layer does usually not incorporate a single kernel, but various kernels that have all the same size, but differ in values, producing various feature maps. In consequence, the set of kernels can be understood as a four-dimensional tensor \autocite{Goodfellow-et-al-2016}. 

Let the input data be a three-dimensional tensor $\boldsymbol{\mathrm{X}} \in \mathbb{R}^{c_{in} \times h_{in} \times w_{in}}$ and let the set of kernels be a four-dimensional tensor $\boldsymbol{\mathrm{K}} \in \mathbb{R}^{c_{out} \times c_{in} \times k_h \times k_w}$. Then, a convolutional layer performs channel-wise convolution between $\boldsymbol{\mathrm{X}}$ and $\boldsymbol{\mathrm{K}}$ and eventually adds a bias tensor $\boldsymbol{\mathrm{B}} \in \mathbb{R}^{c_{out} \times h_{out} \times w_{out}}$, resulting in the three-dimensional tensor $\boldsymbol{\mathrm{Z}} \in \mathbb{R}^{c_{out} \times h_{out} \times w_{out}}$:

\begin{equation}
    \boldsymbol{\mathrm{Z}}_{i, j, l} = \sum_a^{c_{in}} \left(\boldsymbol{\mathrm{X}}_a * \boldsymbol{\mathrm{K}}_{i, a}\right)_{j, l} + \boldsymbol{\mathrm{B}}_{i, j, l} = \sum_a^{c_{in}} \sum_b^{k_h} \sum_d^{k_w} \boldsymbol{\mathrm{X}}_{a, j-b, l-d} \cdot \boldsymbol{\mathrm{K}}_{i, a, b, d} + \boldsymbol{\mathrm{B}}_{i, j, l}.
    \label{eq:convolutional_layer}
\end{equation}

For both types of layers, a non-linear activation function is applied element-wise to the result $\boldsymbol{\mathrm{z}}$ respectively $\boldsymbol{\mathrm{Z}}$. A common one is the \ac{relu} function, which clips negative values to zero:

\begin{equation}
    ReLU(z) = max(0, z).
    \label{eq:relu}
\end{equation}

On the output layer of a classifier, the Softmax function can be used as an activation function to achieve a categorical probability distribution (also called Multinoulli distribution), meaning values of output layer's units are between zero and one, while all these values sum up to one. The Softmax function is defined as

\begin{equation}
    Softmax(\boldsymbol{\mathrm{z}}) = \frac{\exp{\boldsymbol{\mathrm{z}}}}{\sum \exp{\boldsymbol{\mathrm{z}}}}.
    \label{eq:softmax}
\end{equation}

Opposite to fully connected layers, convolutional layers in many cases perform another operation, called pooling. Pooling basically reduces the size of feature maps by mapping several values onto a single one. Similar to convolution, a fixed-shape window is slid over all regions of the activated feature maps, creating views. But instead of computing a weighted sum, a reducing operation is performed on the view. For example, $n \times n$-average-pooling takes the average value from each $n \times n$ view onto the feature maps \autocite{Goodfellow-et-al-2016}.

Last, for efficiency reasons, often not a single data unit, here a single 2D RGB image, is passed through a neural network, but a batch of them. As a consequence, the described input and output tensors are extended by another dimension. 


\subsubsection{Chosen Neural Network Architecture}

While designing the neural network for traffic sign classification, several frame conditions have to be taken into account. First, $32 \times 32$ px RGB images act as input data. Second, six classes are to be differentiated. Third, computational resources on the NVIDIA Jetson Nano are limited, although having a dedicated \ac{gpu} and providing the TensorRT library for optimization of computations.

Benchmarks of the NVIDIA Jetson Nano have proven that the system can run a TensorRT optimized 50 layers deep standard ResNet at 36 Hz in average for a single $224 \times 224$ px image per forward pass \autocite{JetsonNanoDeep2019}. While actually several traffic sign candidates should be classified in a single forward pass, input images only have a resolution of $32 \times 32$ px, which is assumed to balance each other. Ultimately, the decision fell on \ac{wrn} \autocite{zagoruykoWideResidualNetworks2017} as base architecture, which is an improvement of the classical ResNet \autocite{heDeepResidualLearning2015,heIdentityMappingsDeep2016} architecture. 

The ResNet architecture stands out due to including so-called residual blocks, which are a composition of several, typically convolutional layers. Such a residual block does not pass the sole output of its last layer to the following layer, but adds the input to its first layer to the output of its last layer. Recent studies \autocite{heDeepResidualLearning2015,szegedyInceptionv4InceptionResNetImpact2016} have shown that the ResNet architecture increases both the convergence speed during training and the final model performance. However, further increasing the depth of neural network goes along with problems like gradient vanishing \autocite{hochreiterSeppHochreiterThesis1991} during training.

The \ac{wrn} architecture uses less residual blocks and hence layers in total, but in turn increases the width of the layers. \Citeauthor{zagoruykoWideResidualNetworks2017} \autocite{zagoruykoWideResidualNetworks2017} have shown that a 16 layers deep \ac{wrn} with widen factor 8 (\ac{wrn}-16-8) achieves comparable performance to a 1001 layers deep standard ResNet (ResNet-1001), while being significantly easier to be trained. 

A residual block in the \ac{wrn} architecture is built up from two convolutional layers that are preceded by batch normalization and ReLU activation, respectively. Batch normalization \autocite{ioffeBatchNormalizationAccelerating2015} is a technique to increase training speed and make training progress more stable in general by normalizing layers' input data every batch. In addition, dropout is performed between the first convolutional layer and the second batch normalization. Dropout \autocite{srivastavaDropoutSimpleWay2014} randomly discards some output data of the previous layer during training, resulting in more robust models that are less prone to overfitting. Starting at a default width, the aforementioned widen factor represents a multiplier for the layer widths, i.e. the number of feature maps created by convolutional layers.

Several of these residual blocks are composed to a top level block, whose size depends on the total number of layers the neural network should contain. Due to the high learning capabilities of the \ac{wrn} architecture and the comparatively small number of classes to be differentiated, the minimum depth of 10 layers was chosen, leading to top level blocks that consist of a single residual block. Moreover, the widen factor was set to 4. The resulting \ac{wrn}-10-4 contains three of these top level blocks, being preceded by another, statically sized convolutional layer that acts as first layer after the input layer. The top level blocks are followed by an $8 \times 8$-average-pooling. Last, a fully connected layer marks the output layer of the neural network, using Softmax as activation function. \Cref{fig::wrn_10_4_architecture} shows the architecture of the used \ac{wrn}-10-4.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{Pictures/wrn_10_4.pdf}
    \caption[The architecture of \acs{wrn}-10-4]{The architecture of \ac{wrn}-10-4. The chart shows a forward pass through the neural network as a block diagram, with the input layer colored gray, convolutional layers colored orange, fully connected layers colored magenta, batch normalization colored green, \ac{relu} and Softmax activation colored yellow, dropout colored purple, and pooling colored blue.}
    \label{fig::wrn_10_4_architecture}
\end{figure}


\subsection[Neural Network Training]{Neural Network Training $\ddagger$} \label{ch::nn_training}

\subsubsection{Fundamentals}

Training a neural network means gradually adapting its parameters $\boldsymbol{\mathrm{\theta}}$ in such a way that the discrepancy between model's output and the objectively correct output becomes minimal for all inputs of the considered data distribution. The aim of training is that the neural network achieves a high performance, e.g. being measured by the accuracy metric.

However, since accuracy is basically a binary metric (either a specific prediction is correct or it is not), it is not suitable to fine tune parameters as detailed as being necessary. By contrast, a loss function $J$ provides an indirect performance measurement, mapping the discrepancy of a model's output $\boldsymbol{\mathrm{\hat{y}}}$ and the corresponding ground truth data $\boldsymbol{\mathrm{y}}$ onto a real number:

\begin{equation}
    J: \mathbb{R}^n \times \mathbb{R}^n \to \mathbb{R}.
    \label{eq:loss_func}
\end{equation}

A loss function hence takes the complete output data into account, rather than just the binary result of being a match or not. The smaller the discrepancy between $\boldsymbol{\mathrm{\hat{y}}}$ and $\boldsymbol{\mathrm{y}}$, the lower the loss function becomes. Thus, optimization of the model parameters $\boldsymbol{\mathrm{\theta}}$ can be achieved by minimization of the loss function over the considered data distribution, being modeled by the training dataset $p_{data}$ \autocite{Goodfellow-et-al-2016}:

\begin{equation}
    \argmin_{\boldsymbol{\mathrm{\theta}}}{\mathbb{E}_{(\boldsymbol{\mathrm{x}}, \boldsymbol{\mathrm{y}}) \sim p_{data}} J(\hat{f}(\boldsymbol{\mathrm{x}}), \boldsymbol{\mathrm{y}})}.
    \label{eq:loss_minimization}
\end{equation}

The loss function can be extended by a parameter norm penalty, regularizing the parameter adaption to prevent overfitting. For example, $L^2$ regularization adds the $L^2$ norm of the current parameter vector $\boldsymbol{\mathrm{\theta}}$ to the loss function, penalizing high absolute parameter values that may create highly specialized models. With $L^2$ regularization, the composite loss function results in

\begin{equation}
    J(\boldsymbol{\mathrm{\hat{y}}}, \boldsymbol{\mathrm{y}}) + \gamma\cdot\frac{1}{2}\|\boldsymbol{\mathrm{w}}\|_2^2,
    \label{eq:loss_func_l2}
\end{equation}

where $\gamma$ denotes the regularization strength and $\boldsymbol{\mathrm{w}}$ denotes the parameter vector without any bias parameters. The bias parameters are unregularized to avoid underfitting \autocite{Goodfellow-et-al-2016}.

For multinomial classification, cross-entropy is often used as a loss function. Cross-entropy is a statistical measure that makes a statement about the dissimilarity between two probability distributions \autocite{murphyMachineLearningProbabilistic2012}.\footnote{In general, cross-entropy is not symmetrical, i.e. $\mathbb{H}(\boldsymbol{\mathrm{p}}, \boldsymbol{\mathrm{q}}) \neq \mathbb{H}(\boldsymbol{\mathrm{q}}, \boldsymbol{\mathrm{p}})$ \autocite{murphyMachineLearningProbabilistic2012}. However, in terms of cross-entropy loss, only the first term is needed.} Let $\boldsymbol{\mathrm{p}} \in \mathbb{R}^n$ and $\boldsymbol{\mathrm{q}} \in \mathbb{R}^n$ be two discrete probability distributions. Then, the cross entropy of $\boldsymbol{\mathrm{q}}$ w.r.t. $\boldsymbol{\mathrm{p}}$ is defined as

\begin{equation}
    \mathbb{H}(\boldsymbol{\mathrm{p}}, \boldsymbol{\mathrm{q}}) = -\sum_i{p_i\cdot\log{q_i}}.
    \label{eq:cross_entropy}
\end{equation}

Since in multinomial classification, the output layer returns a discrete probability distribution $\boldsymbol{\mathrm{\hat{y}}}$ and the ground truth data $\boldsymbol{\mathrm{y}}$ can be interpreted as fixed reference probability distribution, cross entropy loss is analogously defined as

\begin{equation}
    J_{ce}(\boldsymbol{\mathrm{\hat{y}}}, \boldsymbol{\mathrm{y}}) = -\sum_i{y_i\cdot\log{\hat{y}_i}}.
    \label{eq:cross_entropy_loss}
\end{equation}

The activation functions of a neural network are non-linear, so the loss function w.r.t. $\boldsymbol{\mathrm{\theta}}$ is non-convex. In order to minimize such a non-convex function, one can use gradient descent, if the function is differentiable. In gradient descent, the gradient of the loss function w.r.t. the model parameters is gradually subtracted from the current parameter vector $\boldsymbol{\mathrm{\theta}}^{(i)}$, beginning at an either randomly or purposely chosen initial parameter vector $\boldsymbol{\mathrm{\theta}}^{(0)}$ and eventually converging at a minimum: 

\begin{equation}
    \boldsymbol{\mathrm{\theta}}^{(i+1)} = \boldsymbol{\mathrm{\theta}}^{(i)} - \alpha \cdot \nabla_{\boldsymbol{\mathrm{\theta}}} J.
    \label{eq:gradient_descent}
\end{equation}

Here, $\alpha$ denotes the so-called learning rate, which scales the gradient and avoids leaping over the minimum. It is usually reduced as the training process progresses, leading to local optimization rather than further exploration of the parameter space. Unfortunately, non-convexity implies that gradient descent does not necessarily converge at the global minimum, but can converge at local minima as well \autocite{bertsekasNonlinearProgramming2016}. Moreover, gradient descent can get stuck at saddle points and plateaus of the loss surface, causing insufficient parameter adaption. However, there exist several sophisticated variants of gradient descent that can reduce risk for convergence at those bad critical points, being capable of escaping from there. In addition, \citeauthor{choromanskaLossSurfacesMultilayer2015} \autocite{choromanskaLossSurfacesMultilayer2015} have shown that most local minima are sufficient, while the global minimum often raises overfitting.

Because numerical computation of the gradient $\nabla_{\boldsymbol{\mathrm{\theta}}} J$ through the difference quotient is computationally too expensive\footnote{The loss function would have to be evaluated once for every single model parameter. Every evaluation includes a separate forward pass through the neural network.}, it cannot be used. The backpropagation algorithm, on the contrary, allows efficient and analytical computation of the gradient, using the chain rule of analysis. Assuming that every mathematical operation being part of the network's forward pass can be expressed as a differentiable function, the loss function can be seen as a composition of these functions, being differentiable itself. Then, starting from the loss function, partial differentials w.r.t. intermediate results of hidden layers can be computed by the aid of the chain rule, called backward pass. As the partial differentials between those intermediate results and the parameters being involved in their computation are known, partial differentials of the loss function w.r.t. these parameters and therefore the gradient can ultimately be retrieved from appropriate multiplication of differentials \autocite{nielsenNeuralNetworksDeep2015}.

Classical gradient descent on the one hand incorporates the whole training dataset per optimization step. In view of large datasets, such a forward and backward pass can be computationally very expensive, slowing down the training process. \ac{sgd} on the other hand only takes a subset of the whole training dataset, called mini-batch, per optimization step. Since these subsets are not necessarily representative for the considered data distribution, the computed gradients are only an approximation of the actual gradients and tend to be noisy, especially for small batch sizes. Therefore, the progress of minimization is not steady. But only due to the noisy gradients, \ac{sgd} is capable of escaping from local minima and other bad critical points \autocite{Goodfellow-et-al-2016}. 

Last, the actual training process is organized in terms of epochs. An epoch is the utilization of the complete training data set for loss minimization, regardless of the batch size and the resulting number of minimization steps. After an epoch, the loss is evaluated for the test dataset as well. If a significant gap arises between training loss and test loss, i.e. the training loss is continuously decreased with the test loss remaining on a constant higher level, this indicates overfitting and training should be stopped \autocite{Goodfellow-et-al-2016}.


\subsubsection{Performing the Training}

The previously designed \ac{wrn}-10-4 was trained for 200 epochs on the handcrafted traffic sign training dataset. The batch size was set to 32, representing a good compromise between training speed and effectivity. Furthermore, at the beginning of each epoch, the training dataset was shuffled in order to eliminate the risk of order memorization. 

To increase the diversity of the training dataset, training data was augmented dynamically during training, i.e. purposefully manipulated through image transformations. Data augmentation is known to reduce the risk of overfitting \autocite{shortenSurveyImageData2019}, but as any kind of data manipulation can have unintended side effects, one has to be careful and take the frame conditions into account. For example, commonly used horizontal flipping cannot be applied here, since the turn left and turn right signs are symmetrical w.r.t. the vertical center axis. In the end, augmentation was limited to image expansion by 4 px in each direction using a reflection algorithm and subsequent random cropping back to $32 \times 32$ px.

Due to the given classification task, cross-entropy was used as a loss function. As an optimization algorithm, AdamW \autocite{loshchilovDecoupledWeightDecay2019} was chosen, which is an improved version of the popular \ac{adam} \autocite{kingmaAdamMethodStochastic2017} algorithm. Adam is a variant of \ac{sgd} which uses exponentially moving averages of the gradient's first and second order moments instead of the gradient itself to perform minimization steps. Since the resulting step vector does not only contain information about the originally next step, according to the current gradient, but also about previous steps, it is very likely to escape e.g. from local minima and further difficult regions of the loss surface. \Citeauthor{loshchilovDecoupledWeightDecay2019} \autocite{loshchilovDecoupledWeightDecay2019} subsequently showed that $L^2$ regularization is not beneficial, but rather leads to worse model performance in combination with \ac{adam}. Moreover, they proved that $L^2$ regularization and weight decay, which is primarily another regularization approach, are not equivalent in terms of \ac{adam}, as they are for classical \ac{sgd}. In consequence, they invented the AdamW algorithm, which decouples weight decay from the \ac{adam}-based optimization step and enables effective regularization. The parameters $\beta_1$ and $\beta_2$ of \ac{adam} were set to their default values from the original paper \autocite{kingmaAdamMethodStochastic2017}, $\beta_1=0.9$ and $\beta_1=0.999$, and the weight decay factor was set to 0.01.

The initial learning rate was set to 0.1, but cosine annealing \autocite{loshchilovSGDRStochasticGradient2017} was applied to decrease the learning rate gradually in the course of training. Despite \ac{adam}'s existing learning rate adaptation capability, \citeauthor{loshchilovDecoupledWeightDecay2019} \autocite{loshchilovDecoupledWeightDecay2019} have also shown that the use of cosine annealing can substantially improve training results here. Finally, the dropout rate was set to 0.3.

The neural network and the training routine were implemented with PyTorch. The training was performed on an own computer system, being equipped with an AMD Ryzen 1600X \ac{cpu}, a NVIDIA GTX 1080 \ac{gpu} and 16GBytes \ac{ram}. It was outsourced to the GPU, to increase the training speed. 

\begin{figure}
    \subcaptionbox{Loss\label{fig::training_loss}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/nn_loss.pdf}}
    \hspace{\fill}
    %
    \subcaptionbox{Accuracy\label{fig::training_acc}}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/nn_accuracy.pdf}}
    \caption[Visualization of training results for \acs{wrn}-10-4]{Visualization of training results for \acs{wrn}-10-4. The left diagram shows the development of loss across the training epochs, the right graph that of accuracy. In both cases, the red graph shows results on the training dataset, the blue one does for the test dataset.}
    \label{fig::training}
\end{figure}

As shown in \cref{fig::training_loss}, after 200 epochs of training, a minimum loss of 0.01 was reached on the training dataset. For the test data set, the final loss of 0.006 was even smaller, pointing out that the model did ultimately not overfit. Apparent fluctuations of the validation loss, e.g. at epoch 110, nevertheless indicate that the model temporarily suffered from overfitting. Last, the development of loss across the training epochs shows that after 125 epochs, only marginal improvements happened. In addition to loss, the development of accuracy during training was also tracked (see \cref{fig::training_acc}). However, it largely coincides with the former. In the end, accuracies of 99.67\% (training data) respectively 100\% (test data) were achieved.


\subsection[Neural Network Acceleration]{Neural Network Acceleration $\ddagger$} \label{ch::nn_acceleration}

The trained PyTorch model is to be integrated in the software frame of the \ac{ai}-controlled vehicle later on. Thus, inference has to be fast enough to not lower the working frequency too much. A benchmark of the original PyTorch \ac{wrn}-10-4 on the NVIDIA Jetson Nano with batch size 8 and 3000 repetitions yields 22.76ms in average, with a standard deviation of 26.42ms. The chosen batch size might wonder, but one has to keep in mind that the recognition of traffic sign candidates is not free of failure, possibly providing false-positive candidates. However, the measured durations, in particular in view of their significant fluctuations, are too high in order to achieve a stable working frequency of about 30Hz. In consequence, the model needs to be accelerated with optimizations. NVIDIA provides the TensorRT \ac{sdk} \autocite{tensorrt} to optimize neural networks for CUDA-based systems. TensorRT was available on the NVIDIA Jetson Nano due to the installation of Jetpack \autocite{nvidiaJetPackSDK2014}.

TensorRT performs various optimizations, one of which is the reduction of floating point precision from single precision to either half precision (16 bits) or quarter precision (8 bits). Here, one has to ensure that precision loss does not cause misclassification. Further optimizations are e.g. fusion of operations or improvements in terms of memory allocation \autocite{tensorrt}.

The TensorRT \ac{sdk} itself does not support optimization of PyTorch models natively, but NVIDIA has also developed the torch2trt converter \autocite{NVIDIAAIIOTTorch2trt2021}, which is a Python module that enables the needed conversion. Although not all mathematical operations of PyTorch are supported, it could be used to optimize the previously trained \acs{wrn}-10-4 in terms of speed.

Eventually, 16 bits floating point optimization was used. Test-wise inference on both the original PyTorch model and the optimized TensorRT model yielded a maximum difference of 0.16\% per class probability on the output layer. This discrepancy was considered to be acceptable, as the accuracy evaluation on the test dataset showed 100\% hit ratio and trajectory planning decisions are only to be taken if the class probability is greater than a certain threshold (see \cref{ch::nn_inference}). Besides, the gained execution speed due to TensorRT optimization was too important to be discarded: Applying the same benchmark that was used for the original PyTorch model to the optimized \ac{wrn}-10-4, inference takes about 7.46ms in average with a standard deviation of 1.77ms. Therefore, the accelerated neural network can be integrated into the software frame without throttling the system.

\subsection[Inference Test]{Inference Test $\ddagger$} \label{ch::nn_inference}

In order to test the traffic sign classification in the actual application environment, inference logic was integrated into the software frame at first, namely the Traffic Sign Recognition process. But before performing the test, the question had to be answered whether all classifications should be factored in at all.

\begin{figure}
    \centering
    \includegraphics[width=0.8\textwidth]{Pictures/nn_classification_stop.png}
    \caption[Correct classification of the stop sign]{Correct classification of the stop sign. However, being placed not directly in front of the camera often leads to misclassification.}
    \label{fig::nn_classification_stop}
\end{figure}

Since there are six classes to be differentiated, by-chance hit ratio is about 16.67\%. Hence, simply taking the class with the highest probability on the output layer was not considered useful: The closer class probabilities are to that minimum by-chance probability, the more classification results tend to fluctuate, leading to high frequency changing decisions in trajectory planning. Moreover, one does understandably not want to e.g. drive past a traffic light that has been classified as green with only 30\% certainty. Thus, only the traffic signs that were classified with a sufficiently high certainty are to be factored in trajectory planning. On the other hand, this minimum certainty should not be set too high with the number of accepted classifications becoming too low to perform correct trajectory planning. In an attempt to find a good trade-off between the greatest possible classification certainty and the minimum necessary frequency of accepted classifications, 60\% was finally chosen as the threshold.

In the inference test, the AI-controlled vehicle was placed on a test track with traffic signs and traffic lights alongside it. The vehicle was moved along the track through the functionality of manual control, classifications were meanwhile tracked on the remote \ac{gui}.

The evaluation of the test results indicated that the implemented traffic sign classification is not sufficiently robust against variable environment conditions. Both changing backgrounds and light conditions lead to frequent misclassification; for example, the stop sign is constantly misclassified when being placed alongside the track. However, it is in most cases correctly classified when being placed centrally on track, in front of the vehicle and hence being illuminated directly by the vehicle's front light, as shown in \cref{fig::nn_classification_stop}.

In addition, there are some traffic signs respectively light states which are misclassified in any test scenario built up in the application environment, namely the red traffic light and the turn right sign, being steadily classified as turn left signs (see \cref{fig::nn_misclassification}).

\begin{figure}
    \subcaptionbox{Red traffic light}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/nn_classification_red.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Turn right sign}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/nn_classification_right.png}}
    \caption[Misclassification of the red traffic light and the turn right sign]{Misclassification of the red traffic light and the turn right sign. Both are classified as turn left sign.}
    \label{fig::nn_misclassification}
\end{figure}

Last, there are also classes which are classified correctly at least sometimes, when being placed next to the lanes. If the vehicle is placed statically, both the green traffic light and the turn left sign are often classified with a certainty over 80\%, provided the signs are recognized. \Cref{fig::nn_correct_classification} shows the described results by means of two example pictures. However, moving the vehicle leads to frequent misclassification as well, probably caused by changing backgrounds. The yellow traffic light showes similar behavior, but is even less likely to be classified correctly.

\begin{figure}
    \subcaptionbox{Green traffic light}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/nn_classification_green.png}}
    \hspace{\fill}
    %
    \subcaptionbox{Turn left sign}%
    {\includegraphics[width=0.45\textwidth, keepaspectratio]{Pictures/nn_classification_left.png}}
    \caption[Correct classification of the green traffic light and the turn left sign]{Correct classification of the green traffic light and the turn left sign.}
    \label{fig::nn_correct_classification}
\end{figure}

In retrospect, the created training and test datasets are too small for proper training of the neural network and successful image classification (1142 images in total, see \cref{ch::nn_dataset}). At the same time, uneven class distribution can be excluded as a cause of suspiciously frequent misclassification in favor the turn left sign, since the relative distribution of the dataset is approximately uniformly. In addition, particularly the slightly more frequently represented classes (stop sign, red and yellow traffic light) are those that were frequently misclassified, although the neural network should have adapted slightly more towards them. Comparing with popular datasets that have a similar profile, the \ac{cifar}-10 dataset \autocite{krizhevskyLearningMultipleLayers} provides 60000 $32 \times 32$ px RGB images, being divided into 10 classes. Now considering that the traffic sign classification task is easier than \ac{cifar}-10, where e.g. differently colored cars have to be distinguished from differently colored trucks, and that only six instead of ten classes have to be distinguished from each other, it can be assumed that a dataset size of about 30000 images might be sufficient to achieve successful classification in the real application environment. But ultimately, due to the lack of time, there was no possibility to extend the dataset to that effect and perform another test.