%!TEX root = ../report.tex
\section{Trajectory Planning} \label{ch::trajectory_planning}
In this section the composition of detected lanes, classified signs and measured distances to steering and acceleration signals is described. 

If the autonomous driving mode is enabled, a cyclic worker method fetches all data and converts it separately into control signals.
An exception is made when the user decides to take manual control of the car, in which case no safety precautions are active and no trajectory data is computed.\\
Data acquired by proximity sensors is prioritized over recognized traffic signs, which furthermore are prioritized over detected lanes. Not only the recent data is included in the calculations, but also the previously applied values in order to stabilize the driving behavior.

Finally, the calculated steering value, which corresponds to the servo angle, as well as the speed value, which is similar to the \ac{pwm} angle to control the \ac{esc}, are sent to the specific components via the \ac{i2c} bus. \\
Max-min-construct are used to keep the calculated steering and speed parameters within the safety boundaries.
Equation \ref{eq::max_min_steering} shows this for the steering angle:

\begin{equation} \label{eq::max_min_steering}
    \text{steering} = max(max_{left}, min(max_{right}, angle_{current}))
\end{equation} 

On termination, the car's software ensures that both engine and steering servo are set to their neutral positions.   
As one can see in figure \ref{fig::task_sequence_plan} the worker method is called every time, when all data is ready.  

\subsection[Lane Based Trajectory Planning]{Lane Based Trajectory Planning $\ast$} \label{ch::trajectory_lanes}
As described in section~\ref{ch::lane_detection} the detected lanes are returned as an array of four element arrays, holding the pixel coordinates of the start and end points for each line.

Analogue to the joining of lines, in trajectory planning the detected lines are also split into horizontal and vertical lines.
Afterwards, steering and speed parameters are computed separately for both groups.
Thus, fusing these parameters is necessary. \\
A trajectory vector, to be more specific a direction vector, is used as intermediate format to simplify computations.
The direction of this vector matches the steering parameter, while the length corresponds to the speed parameter. \\
Eventually, the final steering and speed parameters are computed by taking the previously computed values into consideration.

\subsubsection{Vertical Lane Based Trajectory Planning}

First, the lanes are converted to direction vectors, which are required for the subsequent steps.

Due to the non-exact camera calibration for large distances, the position of the lanes in the world coordinate system cannot be solely used for lane based trajectory planning.
However, the distance in the world coordinate system up to a specific threshold contributes weighted to the result. \\
Thus, based on the assumption, that lanes close to the car are detected more reliably, than more distant lanes, the detected lanes are also weighted by their length in the image. \\
Experiments show, that for the presented setup weights of $70\%$ for the length and $30\%$ for the real-world position are most suitable.

Afterwards, the composed weights are multiplied with the initially calculated direction vectors, to generate one vertical lane based trajectory vector. Finally, this vector is scaled to a length of 1.

\subsubsection{Horizontal Lane Based Trajectory Planning}

If a horizontal lane is detected in front, the pixel coordinate of its center $y_{center}$ is computed as the average of the y values of start and end point.
Afterwards, the image point $(\lfloor\frac{width}{2}\rfloor, y_{center})$  -- $width$ denotes the image width -- is transformed to the world coordinate system. \\
The y coordinate of the determined object point, which corresponds to the distance of the lane, is finally compared against a threshold distance. \\
If the distance exceeds this constant, the horizontal lane is ignored and if no other horizontal lane is detected the trajectory vector $(0, 1)$ is returned.
Otherwise, a trajectory vector pointing in y direction is returned.
The length $l$ of this vector denotes the distance of the closest horizontal lanes and guides the trajectory planning to decrease the speed in case of $0 < l < 1$ or to stop the engine in case of $l = 0$.

\subsubsection{Fusion to Lane Based Trajectory Planning}

Finally, the separately determined trajectory vectors need to be combined to one.
Due to the dependence of the motor's torque on the battery charge and the resulting varying response threshold of the motor, the speed is not reduced by the calculated horizontal trajectory vector to avoid undesired stopping.

If the calculated vertical trajectory vector is approximately vertical, i.e. does not indicate a strong change of the steering angle, the horizontal trajectory vector, if available, is returned.
Otherwise, the vertical trajectory vector is returned as fusion of both.
However, if a curve is recognized the vertical trajectory vector is returned.

\subsubsection{Computation of Final Values}

Now, the computed trajectory vector needs to be converted to values suitable for steering and speed control.
Therefore, a mapping is introduced, which determines the speed from the length of this vector and the steering angle from the vector's slope.

If strong steering is required, the computed \ac{pwm} angle for the \ac{esc}, i.e. the speed parameter is reduced by a few steps.

Subsequently, however, the calculated values are not passed on directly to the components, but are combined with the previous control values.
Here, the last 10 values, separately for steering and speed, are linear weighted with a moving average. \\
In fact, this calculation method originates from the financial sector and is used to support investment decisions based on trend of prices \cite{basurto_python_2020}. 
However, it also showed very good test results for the application on weighting consecutive control signals.

Thereby, the weight for the i-th of in total 10 elements can be computed by equation~\ref{eq:lwma}.
\begin{equation}
    \begin{gathered}
        w_{10}(i) = \frac{2 \cdot i}{10 \cdot (10 + 1)}  
    \end{gathered}
    \label{eq:lwma}
\end{equation}

Thus, the most recent elements have significantly higher weights than older elements, but they are still taken into consideration.
This weighting results in smooth and accurate steering, but depending on the battery charge, leads to step-wise acceleration in sharp curves.

\subsection[Traffic Sign Based Trajectory Planning]{Traffic Sign Based Trajectory Planning $\dagger$} \label{sec::trajectory_signs}
Since, as mentioned in section~\ref{ch::traffic_sign_classification}, traffic sign classification does not work reliably under real conditions, the elaborated concept for control planning based on traffic signs could not be tested yet. 
Nevertheless, its theory will be presented in the following.

Each traffic sign has a different impact on the trajectory planning. Only the most important sign is included in the calculations. This weighting is made up of the classification accuracy and the general importance of the sign, which can be obtained from table \ref{tab::traffic_sign_weights} and is calculated as the product of both values.

\begin{table}
    \begin{tabularx}{\textwidth}{|X|X|}
        \hline
        \textbf{Traffic Sign Type} & \textbf{Importance} \\\hline
        Light Green & 0.7 \\\hline
        Light Red & 1.0 \\\hline
        Light Yellow & 0.9 \\\hline
        Stop Sign & 1.0 \\\hline
        Turn Left & 0.8 \\\hline
        Turn Right & 0.8 \\\hline
        None & 0.0 \\\hline
    \end{tabularx}
    \caption[Traffic Signs weighting map]{Map of traffic sign types weightings reaching from 0.0 (unimportant -- no impact) to 1.0 (very important -- high impact)}
    \label{tab::traffic_sign_weights} 
\end{table}

\begin{description}
    \item[Light Green]: A green traffic light results in no steering advice, but the car is told to keep its speed.
    \item[Light Yellow]: The car is advised to reduce its speed, as the light may switch to red. 
    \item[Light Red]: The car stops and waits for the light to switch to green. Like the stop sign the red light has a high weight. 
    \item[Stop Sign]: The car has to stop, when such a sign is encountered. The distance is detached from the recognition of the sign and based on the recognition of the horizontal lane next to the sign, on the street.\\
    However, this method determines the timing of starting the car and driving over the horizontal lane by disabling its influence on the speed parameter.  
    \item[Turn Left / Right]: The speed is reduced and the steering angle is manipulated in the respective direction, which \glqq pushes\grqq ~the car into the bifurcation shown by the sign. Then the lane based trajectory can take sole control.
\end{description}

\subsection[Proximity Sensor Based Trajectory Planning]{Proximity Sensor Based Trajectory Planning $\dagger$} \label{sec::trajectory_ultrasonic}

If an obstacle appears in the area around the car, it should stop to avoid damage and wait for a clear track. For this purpose the ultrasonic sensor data has a direct impact on the speed and is prioritized over lane and traffic sign data. 

Since, as mentioned in section~\ref{ch::distance_measuring}, only the front sensor is used, only obstacles in forward direction are detected.
If a distance less than $30$cm is measured by the ultrasonic sensor\footnote{According to the data sheet\cite{hcsr04} and performed measurements (see section \ref{ch:ultrasonic_accuracy}) lower distances lead to inaccurate results.}, the engine is stopped immediately and the trajectory planning based on lanes and traffic signs is skipped.

As long as distances larger than $30$cm are measured, the ultrasonic sensor data has no impact on the trajectory planning.
