from graphviz import Digraph

def dag():
    dag = Digraph()
    
    dag.node("0", "Image Acquisition")
    dag.node("1", "Traffic Sign Recognition")
    dag.node("2", "Lane Detection")
    dag.node("3", "Collision Detection")
    dag.node("4", "Vehicle Controlling")
    dag.node("5", "Data Receiving")
    dag.node("6", "Data Sending")
    dag.attr('node', shape='circle')
    dag.node("start", "")
    dag.attr('node', shape='doublecircle')
    dag.node("end", "", fillcolor="black", style="filled")
    
    dag.edge("start", "0")
    dag.edge("0", "1")
    dag.edge("0", "2")
    dag.edge("1", "4")
    dag.edge("2", "4")
    dag.edge("3", "4")
    dag.edge("4", "6")
    dag.edge("5", "end")
    dag.edge("6", "end")
    
    dag.render('dag.gv', view=True)
    
def dg():
    dg = Digraph()
    
    dg.node("0", "Image Acquisition")
    dg.node("1", "Traffic Sign Recognition")
    dg.node("2", "Lane Detection")
    dg.node("3", "Collision Detection")
    dg.node("4", "Vehicle Controlling")
    dg.node("5", "Data Receiving")
    dg.node("6", "Data Sending")
    dg.attr('node', shape='circle')
    dg.node("start", "")
    dg.attr('node', shape='doublecircle')
    dg.node("end", "", fillcolor="black", style="filled")
    
    dg.edge("start", "0")
    dg.edge("0", "1")
    dg.edge("0", "2")
    dg.edge("1", "4")
    dg.edge("2", "4")
    dg.edge("3", "4")
    dg.edge("4", "6")
    dg.edge("5", "5")
    dg.edge("5", "end")
    dg.edge("6", "0")
    
    dg.render('dg.gv', view=True)
    
def dg_enhanced():
    dg_en = Digraph()
    
    dg_en.node("0", "Image Acquisition")
    dg_en.node("1", "Traffic Sign Recognition")
    dg_en.node("2", "Lane Detection")
    dg_en.node("3", "Collision Detection")
    dg_en.node("4", "Vehicle Controlling")
    dg_en.node("5", "Data Receiving")
    dg_en.node("6", "Data Sending")
    dg_en.node("7", "Copying Data")
    dg_en.attr('node', shape='circle')
    dg_en.node("start", "")
    dg_en.attr('node', shape='doublecircle')
    dg_en.node("end", "", fillcolor="black", style="filled")
    
    dg_en.edge("start", "0")
    dg_en.edge("0", "1")
    dg_en.edge("0", "2")
    dg_en.edge("1", "4")
    dg_en.edge("2", "4")
    dg_en.edge("3", "4")
    dg_en.edge("4", "7")
    dg_en.edge("5", "5")
    dg_en.edge("5", "end")
    dg_en.edge("7", "0")
    dg_en.edge("7", "6")
    
    dg_en.render('dg_en.gv', view=True)
    

if __name__ == '__main__':
    dag()
    dg()
    dg_enhanced()